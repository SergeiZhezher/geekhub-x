[![pipeline status](https://gitlab.com/SergeiZhezher/geekhub-x/badges/master/pipeline.svg)](https://gitlab.com/SergeiZhezher/geekhub-x/-/commits/master)
[![coverage report](https://gitlab.com/SergeiZhezher/geekhub-x/badges/master/coverage.svg)](https://gitlab.com/SergeiZhezher/geekhub-x/-/commits/master)

# GeekHub X

## Introduction

> This repository contains 2 projects:
>- First: 'Homework' ( /Homework/ ) - This application is for student registration and create analytics based on their grades.
>- Second: 'Course work'  ( /Examination/libfilm/ ) - Conceived as a movie site that can parse movies from other sites.

> Project 'Homework' contains 4 module:
>- 'console-app' - console version.
>- 'web-app' - web version.
>- 'domain' - general logic.
>- 'oauth2' - independent module, contains an example 'GitLab OAuth2'

## Code Samples

> You can access debug tools with swagger. To do this follow this pattern: http(s)://your-app-root/swagger_url
>- Examples:
>- UI version: http://localhost:8080/swagger-ui/
>- JSON version: http://localhost:8080/v2/api-docs/
> 
> Homework: You can add students using a third party 'students.txt' file containing JSON.
> To do this, just add the file to your home directory.
>- JSON Example:
>- [{"name":"Christian","grade":"GPA","gradeTime":"2077-01-01T10:09:09","gradeValue":"100"},]

## Installation

> Project 'Homework'
>- To run the console version, you need to specify 2 starting arguments: the number of students and the program mode (MANUAL or RANDOM).
   >  For the class:  'Main.java'. Example: '10 RANDOM'.
> 
> 
>- To run the web version, you just need run: 'StudentRegistryApp.java'.

> Project 'Course work'
>-  To run you just need to follow two steps:
> >- First: specify the VM options '-Dspring.profiles.active = dev' for LibFilmApplication.java.
> >- Second: run LibFilmApplication.java.