package com.libfilm.service;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.libfilm.dao.UserEntity;
import com.libfilm.exception.InvalidCommentException;
import com.libfilm.repository.CommentRepository;
import com.libfilm.repository.FilmRepository;

import static org.junit.jupiter.api.Assertions.assertThrows;

class CommentServiceTest {

    private static final String FILM_COMMENT = "message";
    private static final String FILM_ID = UUID.randomUUID().toString();
    private static final int COMMENT_LENGTH = 1500;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private FilmRepository filmRepository;

    private CommentService commentService;

    private String jsonWithVeryLongComment;
    private String jsonWithEmptyComment;
    private String jsonWithInvalidFilmId;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        commentService = new CommentService(commentRepository, filmRepository);

        String veryLongComment = new String(new char[COMMENT_LENGTH]).replace('\0', ' ');

        jsonWithInvalidFilmId = "{\"comment\" : \"" + FILM_COMMENT + "\",\"filmId\" : \" IS_NOT_UUID \"}";
        jsonWithEmptyComment = "{\"comment\" : \"" + "" + "\",\"filmId\" : \"" + FILM_ID + "\"}";
        jsonWithVeryLongComment = "{\"comment\" : \"" + veryLongComment + "\",\"filmId\" : \"" + FILM_ID + "\"}";
    }

    @Test
    void fail_add_comment_with_empty_comment_test() {
        assertThrows(InvalidCommentException.class, () -> commentService
                .addComment(jsonWithEmptyComment, new UserEntity()));
    }

    @Test
    void fail_add_comment_with_long_comment_test() {
        assertThrows(InvalidCommentException.class, () -> commentService
                .addComment(jsonWithVeryLongComment, new UserEntity()));
    }

    @Test
    void fail_add_comment_with_invalid_film_id_test() {
        assertThrows(InvalidCommentException.class, () -> commentService
                .addComment(jsonWithInvalidFilmId, new UserEntity()));
    }

    @Test
    void fail_add_comment_with_empty_data_test() {
        assertThrows(InvalidCommentException.class, () -> commentService
                .addComment("", new UserEntity()));
    }
}
