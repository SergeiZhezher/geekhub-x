package com.libfilm.util.parser;

import java.util.List;
import java.util.Map;

import org.asynchttpclient.Param;
import org.asynchttpclient.util.HttpConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpHeaders.ACCEPT_ENCODING;
import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpHeaders.REFERER;
import static org.springframework.http.HttpHeaders.USER_AGENT;

class PostFetcherImplTest {

    private static final Map<? extends CharSequence, ? extends Iterable<?>> headers = Map.of(
            ACCEPT_ENCODING, List.of("gzip, deflate, br"),
            ACCEPT_LANGUAGE, List.of("ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"),
            CONTENT_TYPE, List.of("application/x-www-form-urlencoded; charset=UTF-8"),
            REFERER, List.of("https://rezka.ag/"),
            USER_AGENT, List.of("Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Mobile Safari/537.36")
    );

    private PostFetcherImpl postFetcher;

    @BeforeEach
    void setUp() {
        postFetcher = new PostFetcherImpl();
    }

    @Test
    void fetch_post_data_test() {
        String url = "https://rezka.ag/engine/ajax/gettrailervideo.php";

        assertDoesNotThrow(() -> {
            String result = postFetcher.fetch(headers, List.of(new Param("id", "1395")),
                    HttpConstants.Methods.POST, url);

            assertNotNull(result);
        });
    }

}
