package com.libfilm.util.parser;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.libfilm.dao.FilmEntity;
import com.libfilm.util.parser.hdrezka.FilmParserHDREZKA;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FilmParserHDREZKATest {

    private final String filmDescription = "Действия картины разворачиваются спустя десять лет после событий описанных в первой части фильма. Тогда все закончилось благополучно, и у Сары родился сын Джон Конор, которому было предначертано в будущем повести людей на войну против безжалостных роботов.";
    private final String filmLink = "https://rezka.ag/films/fiction/1395-terminator-2-sudnyy-den-1991.html";
    private final String filmTrailerLink = "https://www.youtube.com/embed/JuTg5q83H1g";
    private final String filmName = "Терминатор 2: Судный день";
    private final String filmGenre = "Фантастика";
    private final String filmDuration = "137 мин.";
    private final int filmYear = 1991;

    private static FilmParserHDREZKA filmParserHDREZKA;

    @BeforeAll
    static void setUp() {
        filmParserHDREZKA = new FilmParserHDREZKA(new PostFetcherImpl());
    }

    @Test
    void get_film_link_by_name_from_search_test() {
        assertEquals(filmParserHDREZKA.getFilmLinkByNameFromSearch(filmName), filmLink);
    }

    @Test
    void get_film_data_by_link_test() {
        FilmEntity filmData = filmParserHDREZKA.getFilmDataByLink(filmLink);

        assertEquals(filmData.getDuration(), filmDuration);
        assertEquals(filmData.getGenre(), filmGenre);
        assertEquals(filmData.getYears(), filmYear);

        assertTrue(filmData.getDescription().contains(filmDescription));
        assertTrue(filmData.getTrailerLink().contains(filmTrailerLink));

        assertNotNull(filmData.getRating());
        assertNotNull(filmData.getVotes());
        assertNotNull(filmData.getPosterUrl());
    }

    @Test
    void parse_hints_test() {
        assertDoesNotThrow(() -> {
            List<String> hints = filmParserHDREZKA.parseHints(filmName);

            assertFalse(hints.isEmpty());
        });
    }

}
