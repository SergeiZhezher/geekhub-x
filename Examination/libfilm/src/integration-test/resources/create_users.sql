-- SUPER_ADMIN@gmail.com:SUPER_ADMIN
insert into usr (id, email, password, is_active)
values ('3307b088-a031-11eb-bcbc-0242ac130002', 'SUPER_ADMIN@gmail.com', '$2y$12$ZUP/ui8ev7bca6Q5QDS0f.bScw7iqvwQfwIwPYkdRk.OIeUfBQij6', true);

insert into user_role (user_id, roles)
values ('3307b088-a031-11eb-bcbc-0242ac130002', 'SUPER_ADMIN');


-- user@gmail.com:user
insert into usr (id, email, password, is_active)
values ('4e380b7a-83f1-11eb-8dcd-0242ac130003', 'user@gmail.com', '$2y$12$EM9pGXKJGGxdpYWmXlTIKuWI9nNPHLMoP0bzWR7SSbNaqto3sWTLu', true);

insert into user_role (user_id, roles)
values ('4e380b7a-83f1-11eb-8dcd-0242ac130003', 'USER');