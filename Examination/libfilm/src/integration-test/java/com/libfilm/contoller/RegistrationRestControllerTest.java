package com.libfilm.contoller;

import com.libfilm.config.CommonRestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithAnonymousUser
public class RegistrationRestControllerTest extends CommonRestConfig {

    private static final String P_EMAIL = "email";
    private static final String P_PASSWORD = "password";
    private static final String P_RE_PASSWORD = "confirmedPassword";

    @Test
    void registration_test() throws Exception {
        mockMvc.perform(post("/registration")
                .param(P_EMAIL, "name@gmail.com")
                .param(P_PASSWORD, "123")
                .param(P_RE_PASSWORD, "123")
                .with(csrf()))
                .andExpect(status().isCreated());
    }

    @ParameterizedTest
    @MethodSource("fail_registration_provider")
    void fail_registration_test(String email, String pass, String rePass) throws Exception {
        mockMvc.perform(post("/registration")
                .param(P_EMAIL, email)
                .param(P_PASSWORD, pass)
                .param(P_RE_PASSWORD, rePass)
                .with(csrf()))
                .andExpect(status().isBadRequest());
    }

    static Object[][] fail_registration_provider() {
        return new Object[][]{
                {"invalid email", "123", "123"},
                {"test@gmail.com", "123", "43243"},
                {"test@gmail.com", "", ""},
        };
    }

}