package com.libfilm.contoller;

import com.libfilm.config.CommonRestConfig;
import com.libfilm.dao.FilmEntity;
import com.libfilm.util.parser.hdrezka.FilmParserHDREZKA;
import com.libfilm.util.parser.FilmParserStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
public class FilmManagerRestControllerTest extends CommonRestConfig {

    @MockBean
    private FilmParserHDREZKA filmParserHDREZKA;

    @BeforeEach
    void setUp() {
        when(filmParserHDREZKA.getFilmLinkByNameFromSearch(anyString())).thenReturn("Link");
        when(filmParserHDREZKA.getFilmDataByLink(anyString())).thenReturn(new FilmEntity(
                "Film name", "description", "trailer", "duration", "genre", 2021, 5F, 5F, "poster"
        ));

        when(filmParserHDREZKA.parseHints(anyString())).thenReturn(Collections.emptyList());
    }

    @Test
    void parse_film_test() throws Exception {
        mockMvc.perform(post("/manager/films").param("filmName", "Film name").with(csrf()))
                .andExpect(jsonPath("$", containsString(FilmParserStatus.SUCCESS.name())))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/manager/films").param("filmName", "Film name").with(csrf()))
                .andExpect(jsonPath("$", containsString(FilmParserStatus.FILM_ALREADY_EXIST.name())))
                .andExpect(status().isBadRequest());

        verify(filmParserHDREZKA).getFilmDataByLink(anyString());
        verify(filmParserHDREZKA).getFilmLinkByNameFromSearch("Film name");
    }

    @Test
    void fail_parse_film_with_invalid_name_test() throws Exception {
        mockMvc.perform(post("/manager/films").param("filmName", " ").with(csrf()))
                .andExpect(jsonPath("$", containsString("The name cannot have size less than 3 or contain only a space")))
                .andExpect(status().isBadRequest());

        verify(filmParserHDREZKA, never()).getFilmDataByLink(anyString());
    }

    @Test
    void parse_hints_test() throws Exception {
        mockMvc.perform(post("/manager/films/hints").param("filmName", "Film name").with(csrf()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(filmParserHDREZKA).parseHints("Film name");
    }

    @Test
    void delete_film_test() throws Exception {
        mockMvc.perform(delete("/manager/film").param("filmName", "Film").with(csrf()))
                .andExpect(status().isNoContent());
    }

    @Test
    void fail_delete_film_test() throws Exception {
        mockMvc.perform(delete("/manager/film").param("filmName", " ").with(csrf()))
                .andExpect(status().isBadRequest());
    }

}