package com.libfilm.contoller;

import com.libfilm.config.CommonRestConfig;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class PersonalAreaRestControllerTest extends CommonRestConfig {

    private static final String URL = "/personal-area";

    private static final String TOO_SHORT_PASSWORD = "11";
    private static final String TOO_LONG_PASSWORD = new String(new char[150]).replace('\0', ' ');
    private static final String CURRENT_PASSWORD = "SUPER_ADMIN";
    private static final String NEW_PASSWORD = "MEGA_ADMIN";

    @ParameterizedTest
    @MethodSource({"comments_provider"})
    void get_comments_test(String currentPass, String newPass, String rePass, ResultMatcher serverStatus
    ) throws Exception {
        mockMvc.perform(patch(URL)
                .with(csrf())
                .param("currentPassword", currentPass)
                .param("newPassword", newPass)
                .param("confirmedPassword", rePass)
        ).andExpect(serverStatus);
    }

    static Object[][] comments_provider() {
        return new Object[][]{
                {"ISN'T" + CURRENT_PASSWORD, NEW_PASSWORD, NEW_PASSWORD, status().isBadRequest()},
                {CURRENT_PASSWORD, NEW_PASSWORD, "NOT EQUALS" + NEW_PASSWORD, status().isBadRequest()},
                {CURRENT_PASSWORD, TOO_SHORT_PASSWORD, TOO_SHORT_PASSWORD, status().isBadRequest()},
                {CURRENT_PASSWORD, TOO_LONG_PASSWORD, TOO_LONG_PASSWORD, status().isBadRequest()},
                {CURRENT_PASSWORD, NEW_PASSWORD, NEW_PASSWORD, status().isNoContent()}
        };
    }

}
