package com.libfilm.contoller;

import com.libfilm.config.CommonRestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
public class FilmPageRestControllerTest extends CommonRestConfig {

    private static final String FILM_ID = "3307b088-a031-11eb-bcbc-0242ac130002";
    private static final String FILM_RATING_URL = "/film/rating";
    private static final String P_FILM_ID = "filmId";
    private static final String P_RATING = "rating";

    @Test
    void get_film_page_test() throws Exception {
        mockMvc.perform(get("/film/id/" + FILM_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(FILM_ID)));
    }

    @Test
    void update_rating_test() throws Exception {
        mockMvc.perform(patch(FILM_RATING_URL)
                .param(P_RATING, "5")
                .param(P_FILM_ID, FILM_ID)
                .with(csrf())
        )
                .andExpect(status().isNoContent());
    }

    @Test
    void fail_update_rating_with_invalid_rating_test() throws Exception {
        mockMvc.perform(patch(FILM_RATING_URL)
                .param(P_RATING, "-77")
                .param(P_FILM_ID, FILM_ID)
                .with(csrf())
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    void fail_update_rating_with_not_float_value_test() throws Exception {
        mockMvc.perform(patch(FILM_RATING_URL)
                .param(P_RATING, "IS`NT FLOAT")
                .param(P_FILM_ID, FILM_ID)
                .with(csrf())
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    void fail_update_rating_with_trying_to_rate_twice_test() throws Exception {
        mockMvc.perform(patch(FILM_RATING_URL)
                .param(P_RATING, "5").param(P_FILM_ID, FILM_ID).with(csrf()))
                .andExpect(status().isNoContent());

        mockMvc.perform(patch(FILM_RATING_URL)
                .param(P_RATING, "5").param(P_FILM_ID, FILM_ID).with(csrf()))
                .andExpect(status().isBadRequest());
    }

}
