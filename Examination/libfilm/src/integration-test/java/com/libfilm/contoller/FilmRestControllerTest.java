package com.libfilm.contoller;

import com.libfilm.config.CommonRestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
public class FilmRestControllerTest extends CommonRestConfig {

    private static final String P_MIN_YEAR = "yearMin";
    private static final String P_MAX_YEAR = "yearMax";
    private static final String MIN_YEAR = "2005";
    private static final String MAX_YEAR = "2013";
    private static final String URL = "/films";

    private static final int NUMBER_OF_FILMS_AFTER_FILTER_BY_GENRE_AND_YEARS = 1;
    private static final int NUMBER_OF_FILMS_AFTER_FILTER_BY_GENRE = 3;
    private static final int NUMBER_OF_FILMS_AFTER_FILTER_BY_YEARS = 4;
    private static final int NUMBER_OF_FILM_ON_PAGE = 8;
    private static final int NUMBER_OF_GENRES = 2;
    private static final int NUMBER_OF_HINTS = 5;
    private static final int TOTAL_FILMS = 6;

    @Test
    void get_films_test() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(TOTAL_FILMS)))
                .andExpect(jsonPath("$.pageable.pageSize", is(NUMBER_OF_FILM_ON_PAGE)));
    }

    @Test
    void get_films_with_filter_by_years_test() throws Exception {
        mockMvc.perform(get(URL).
                param(P_MIN_YEAR, MIN_YEAR).param(P_MAX_YEAR, MAX_YEAR))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(NUMBER_OF_FILMS_AFTER_FILTER_BY_YEARS)));
    }

    @Test
    void get_films_with_filters_by_years_and_genre_test() throws Exception {
        mockMvc.perform(get(URL + "/genre/Comedy").
                param(P_MIN_YEAR, MIN_YEAR).param(P_MAX_YEAR, MAX_YEAR))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(NUMBER_OF_FILMS_AFTER_FILTER_BY_GENRE_AND_YEARS)));
    }

    @Test
    void fail_get_films_with_invalid_filters_test() throws Exception {
        mockMvc.perform(get(URL).
                param(P_MIN_YEAR, "IS`NT INTEGER").param(P_MAX_YEAR, "IS`NT INTEGER"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(TOTAL_FILMS)));
    }

    @Test
    void get_films_by_genre_test() throws Exception {
        mockMvc.perform(get(URL + "/genre/Comedy"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(NUMBER_OF_FILMS_AFTER_FILTER_BY_GENRE)))
                .andExpect(jsonPath("$.pageable.pageSize", is(NUMBER_OF_FILM_ON_PAGE)));
    }

    @Test
    void get_film_genres() throws Exception {
        mockMvc.perform(get(URL + "/genres"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(NUMBER_OF_GENRES)));
    }

    @Test
    void get_hints_test() throws Exception {
        mockMvc.perform(get(URL + "/hints").param("filmName", "-NN"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(NUMBER_OF_HINTS)));
    }

}
