package com.libfilm.contoller;

import com.libfilm.config.CommonRestConfig;
import com.libfilm.dao.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import java.util.UUID;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
public class UserManagerRestControllerTest extends CommonRestConfig {

    private static final String USER_NAME = "user@gmail.com";
    private static final String ADMIN_NAME = "SUPER_ADMIN@gmail.com";
    private static final String USER_ID = "4e380b7a-83f1-11eb-8dcd-0242ac130003";
    private static final String ADMIN_ID = "3307b088-a031-11eb-bcbc-0242ac130002";

    private static final String P_ID = "id";
    private static final String P_ROLE = "role";
    private static final String P_EMAIL = "email";
    private static final String P_ACTIVE = "active";
    private static final String P_PASSWORD = "password";
    private static final String P_CURRENT_ID = "currentId";
    private static final String P_RE_PASSWORD = "confirmedPassword";

    private static final String URL = "/users";

    @Test
    void get_user_test() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].email", containsInAnyOrder(ADMIN_NAME, USER_NAME)));
    }

    @Test
    void add_user_test() throws Exception {
        mockMvc.perform(post(URL)
                .param(P_EMAIL, "name@gmail.com")
                .param(P_PASSWORD, "123")
                .param(P_RE_PASSWORD, "123")
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Test
    void full_user_update_test() throws Exception {
        mockMvc.perform(put(URL)
                .param(P_EMAIL, "update@gmail.com")
                .param(P_PASSWORD, "new_password")
                .param(P_RE_PASSWORD, "new_password")
                .param(P_CURRENT_ID, USER_ID)
                .with(csrf())
        ).andExpect(status().isNoContent());
    }

    @Test
    void update_role_test() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, USER_ID)
                .param(P_ROLE, Role.ADMIN.name())
                .with(csrf())
        ).andExpect(status().isNoContent());
    }

    @Test
    void update_active_test() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, USER_ID)
                .param(P_ACTIVE, "true")
                .with(csrf())
        ).andExpect(status().isNoContent());
    }

    @Test
    void delete_test() throws Exception {
        mockMvc.perform(delete(URL)
                .param(P_ID, USER_ID)
                .with(csrf())
        ).andExpect(status().isNoContent());
    }

    @Test
    void fail_add_user_without_email_test() throws Exception {
        mockMvc.perform(post(URL)
                .param(P_EMAIL, "")
                .param(P_PASSWORD, "123")
                .param(P_RE_PASSWORD, "123")
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_add_user_with_not_equals_passwords_test() throws Exception {
        mockMvc.perform(post(URL)
                .param(P_EMAIL, "name@gmail.com")
                .param(P_PASSWORD, "123")
                .param(P_RE_PASSWORD, "")
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_add_user_with_already_exist_email_test() throws Exception {
        mockMvc.perform(post(URL)
                .param(P_EMAIL, ADMIN_NAME)
                .param(P_PASSWORD, "123")
                .param(P_RE_PASSWORD, "123")
                .with(csrf())
        ).andExpect(status().isConflict());
    }

    @Test
    void fail_update_data_with_trying_to_update_myself() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, ADMIN_ID)
                .param(P_ROLE, Role.ADMIN.name())
                .with(csrf())
        ).andExpect(status().isForbidden());
    }

    @Test
    void fail_update_data_with_random_id() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, UUID.randomUUID().toString())
                .param(P_ROLE, Role.ADMIN.name())
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_update_role_test() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, ADMIN_ID)
                .param(P_ROLE, "INVALID_ROLE")
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_update_role_with_trying_to_update_super_admin_test() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, ADMIN_ID)
                .param(P_ROLE, "ADMIN")
                .with(csrf())
        ).andExpect(status().isForbidden());
    }

    @Test
    void fail_update_active_test() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, ADMIN_ID)
                .param(P_ACTIVE, "NONE")
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_update_active_with_trying_to_update_super_admin_test() throws Exception {
        mockMvc.perform(patch(URL)
                .param(P_ID, ADMIN_ID)
                .param(P_ACTIVE, "true")
                .with(csrf())
        ).andExpect(status().isForbidden());
    }

    @Test
    void fail_delete_test() throws Exception {
        mockMvc.perform(delete(URL)
                .param(P_ID, ADMIN_ID)
                .with(csrf())
        ).andExpect(status().isForbidden());
    }

    @ParameterizedTest
    @MethodSource("fail_full_user_update_provider")
    void fail_full_user_update_test(String email, String pass, String confirmedPass, String id) throws Exception {
        mockMvc.perform(put(URL)
                .param(P_EMAIL, email)
                .param(P_PASSWORD, pass)
                .param(P_RE_PASSWORD, confirmedPass)
                .param(P_CURRENT_ID, id)
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_full_user_update_with_trying_to_update_super_admin_test() throws Exception {
        mockMvc.perform(put(URL)
                .param(P_EMAIL, ADMIN_NAME)
                .param(P_PASSWORD, P_PASSWORD)
                .param(P_RE_PASSWORD, P_PASSWORD)
                .param(P_CURRENT_ID, ADMIN_ID)
                .with(csrf())
        ).andExpect(status().isForbidden());
    }

    static Object[][] fail_full_user_update_provider() {
        return new Object[][]{
                {"invalid email", "123", "123", USER_ID},
                {USER_NAME, "123", "43243", USER_ID},
                {USER_NAME, "", "", USER_ID},
                {USER_NAME, "123", "123", UUID.randomUUID().toString()}
        };
    }

}
