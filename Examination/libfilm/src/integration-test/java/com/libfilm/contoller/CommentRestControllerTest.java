package com.libfilm.contoller;

import com.libfilm.config.CommonRestConfig;
import com.libfilm.controller.rest.CommentRestController;
import com.libfilm.dao.CommentEntity;
import com.libfilm.dao.UserEntity;
import com.libfilm.repository.CommentRepository;
import com.libfilm.repository.FilmRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class CommentRestControllerTest extends CommonRestConfig {

    private static final String FILM_COMMENT = "message";
    private static final String FILM_ID = UUID.randomUUID().toString();

    @Autowired
    private CommentRestController commentRestController;

    @MockBean
    private SimpMessagingTemplate messagingTemplate;

    @MockBean
    private CommentRepository commentRepository;

    @MockBean
    private FilmRepository filmRepository;

    @Mock
    private Authentication authMock;

    private String jsonParam;

    @BeforeEach
    void setUp() {
        when(authMock.getPrincipal()).thenReturn(new UserEntity());
        when(filmRepository.existsById(any())).thenReturn(true);

        jsonParam = "{\"comment\" : \"" + FILM_COMMENT + "\",\"filmId\" : \"" + FILM_ID + "\"}";
    }

    @Test
    void get_comments_test() throws Exception {
        mockMvc.perform(get("/film/" + UUID.randomUUID() + "/comments"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void comment_sender_test() {
        commentRestController.getComment(jsonParam, authMock);

        verify(messagingTemplate, times(1))
                .convertAndSend(eq("/comment/film/" + FILM_ID), any(CommentEntity.class));
    }
}
