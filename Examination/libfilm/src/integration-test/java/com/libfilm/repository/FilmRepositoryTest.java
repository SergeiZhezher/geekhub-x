package com.libfilm.repository;

import com.libfilm.dao.FilmEntity;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.jdbc.Sql;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;

@DataJpaTest
@Sql(value = {"/create_films.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class FilmRepositoryTest {

    private static final Pageable pageable = PageRequest.of(0, 8);

    private static final int NUMBER_OF_HINTS = 1;
    private static final int NUMBER_OF_FILMS = 5;
    private static final int NUMBER_OF_GENRES = 2;
    private static final int NUMBER_OF_FILMS_AFTER_FILTER_BY_YEARS = 4;
    private static final int NUMBER_OF_FILMS_AFTER_FILTER_BY_YEARS_AND_GENRE = 1;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    void fetch_film_names_by_prefix_test() {
        List<String> names = filmRepository.fetchFilmNamesByPrefix("fir");

        assertThat(names, hasSize(NUMBER_OF_HINTS));
        assertThat(names, containsInAnyOrder("First-NN"));
    }

    @Test
    void fetch_film_genre_test() {
        List<String> genres = filmRepository.fetchFilmGenre()
                .orElseGet(Collections::emptyList);

        assertThat(genres, hasSize(NUMBER_OF_GENRES));
        Assertions.assertThat(genres).doesNotHaveDuplicates();
    }

    @Test
    void find_top_5_all_by_film_name_containing_ignore_case_test() {
        List<FilmEntity> films = filmRepository.findTop5AllByFilmNameContainingIgnoreCase("-nn")
                .orElseGet(Collections::emptyList);

        assertThat(films, hasSize(NUMBER_OF_FILMS));
    }

    @Test
    void find_all_by_years_greater_than_equal_and_years_less_than_equal_test() {
        List<FilmEntity> content = filmRepository.
                findAllByYearsGreaterThanEqualAndYearsLessThanEqual(pageable, 2012, 2015).getContent();

        assertThat(content, hasSize(NUMBER_OF_FILMS_AFTER_FILTER_BY_YEARS));
    }

    @Test
    void find_all_by_years_greater_than_equal_and_years_less_than_equal_and_genre_test() {
        List<FilmEntity> content = filmRepository.
                findAllByYearsGreaterThanEqualAndYearsLessThanEqualAndGenre(pageable, 2012, 2015, "Fantasy")
                .getContent();

        assertThat(content, hasSize(NUMBER_OF_FILMS_AFTER_FILTER_BY_YEARS_AND_GENRE));
    }

}
