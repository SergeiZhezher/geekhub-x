package com.libfilm.secutiry;

import com.libfilm.config.CommonRestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class LoginTest extends CommonRestConfig {

    private static final String SUCCESS_LOGIN_URL = "/";
    private static final String FAILURE_LOGIN_URL = "/?error";
    private static final String LOGOUT_URL = "/login?logout";
    private static final String USER_NAME = "user@gmail.com";
    private static final String USER_PASSWORD = "user";

    @Test
    @WithAnonymousUser
    void correct_login_test() throws Exception {
        mockMvc
                .perform(formLogin().user(USER_NAME).password(USER_PASSWORD))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(SUCCESS_LOGIN_URL))
                .andExpect(authenticated().withUsername(USER_NAME));
    }

    @Test
    @WithAnonymousUser
    void incorrect_login_test() throws Exception {
        mockMvc
                .perform(formLogin().password(USER_PASSWORD + "Invalid"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(FAILURE_LOGIN_URL))
                .andExpect(unauthenticated());
    }

    @Test
    void logout_test() throws Exception {
        mockMvc
                .perform(logout())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(LOGOUT_URL));
    }

}