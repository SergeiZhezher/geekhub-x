package com.libfilm.secutiry;

import com.libfilm.config.CommonRestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class AccessTest extends CommonRestConfig {

    private static final String PERSONAL_AREA_URL = "/personal-area";
    private static final String USER_MANAGER_URL = "/users";
    private static final String FILM_MANAGER_URL = "/manager/films";
    private static final String LOAD_FILMS_URL = "/films";
    private static final String FILM_PAGE_URL = "/film";

    private static final String USER_ID = "4e380b7a-83f1-11eb-8dcd-0242ac130003";
    private static final String MOCK_VALUE = "123";

    @ParameterizedTest(name = "user_manager_access_with_user_test")
    @MethodSource("user_manager_access_provider")
    @WithUserDetails("user@gmail.com")
    void user_manager_access_with_user_test(MockHttpServletRequestBuilder httpMethod) throws Exception {
        mockMvc.perform(httpMethod.with(csrf())
                .param("id", USER_ID)
                .param("currentId", USER_ID)
        ).andExpect(status().isForbidden());
    }

    @ParameterizedTest(name = " user_manager_access_without_authorization_test")
    @MethodSource({"user_manager_access_provider"})
    @WithAnonymousUser
    void user_manager_access_without_authorization_test(MockHttpServletRequestBuilder httpMethod) throws Exception {
        mockMvc.perform(httpMethod.with(csrf())
                .param("id", USER_ID)
                .param("currentId", USER_ID)
        ).andExpect(status().is3xxRedirection());
    }

    @ParameterizedTest(name = "film_manager_access_with_user_test")
    @MethodSource("film_manager_access_provider")
    @WithUserDetails("user@gmail.com")
    void film_manager_access_with_user_test(MockHttpServletRequestBuilder httpMethod) throws Exception {
        mockMvc.perform(httpMethod.with(csrf()).param("filmName", MOCK_VALUE))
                .andExpect(status().isForbidden());
    }

    @ParameterizedTest(name = "film_manager_access_without_authorization_test")
    @MethodSource({"film_manager_access_provider"})
    @WithAnonymousUser
    void film_manager_access_without_authorization_test(MockHttpServletRequestBuilder httpMethod) throws Exception {
        mockMvc.perform(httpMethod.with(csrf()).param("filmName", MOCK_VALUE))
                .andExpect(status().is3xxRedirection());
    }

    @ParameterizedTest(name = "film_page_access_without_authorization_test")
    @MethodSource({"film_page_access_provider"})
    @WithAnonymousUser
    void film_page_access_without_authorization_test(MockHttpServletRequestBuilder httpMethod) throws Exception {
        mockMvc.perform(httpMethod.with(csrf())
                .param("rating", MOCK_VALUE)
                .param("filmId", UUID.randomUUID().toString())
        )
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void personal_area_access_without_authorization_test() throws Exception {
        mockMvc.perform(get(PERSONAL_AREA_URL)
                .param("currentPassword", MOCK_VALUE)
                .param("newPassword", MOCK_VALUE)
                .param("confirmedPassword", MOCK_VALUE)
        )
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void load_films_access_without_authorization_test() throws Exception {
        mockMvc.perform(get(LOAD_FILMS_URL))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void comment_access_without_authorization_test() throws Exception {
        mockMvc.perform(get(FILM_PAGE_URL + UUID.randomUUID() + "comments"))
                .andExpect(status().is3xxRedirection());
    }

    static Object[][] user_manager_access_provider() {
        return new Object[][]{
                {get(USER_MANAGER_URL)},
                {post(USER_MANAGER_URL)},
                {patch(USER_MANAGER_URL)},
                {put(USER_MANAGER_URL)},
                {delete(USER_MANAGER_URL)}
        };
    }

    static Object[][] film_manager_access_provider() {
        return new Object[][]{
                {post(FILM_MANAGER_URL)},
                {post(FILM_MANAGER_URL + "/hints")},
                {delete("/manager/film")}
        };
    }

    static Object[][] film_page_access_provider() {
        return new Object[][]{
                {get(FILM_PAGE_URL + "/id/" + UUID.randomUUID())},
                {patch(FILM_PAGE_URL + "/rating")},
        };
    }

}
