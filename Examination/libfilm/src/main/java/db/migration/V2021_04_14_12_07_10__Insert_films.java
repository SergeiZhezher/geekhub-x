package db.migration;

import com.libfilm.repository.FilmRepository;
import com.libfilm.service.FilmManagerService;
import com.libfilm.util.SpringUtility;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class V2021_04_14_12_07_10__Insert_films extends BaseJavaMigration {

    private static final List<String> filmList = List.of(
            "Бумажный дом", "Викинги", "1917", "Марсианин",
            "Ход королевы / Ферзевый гамбит", "Гарри Поттер и Дары Смерти: Часть II",
            "Форсаж 7", "Форсаж", "Человек-паук: Вдали от дома", "Мстители: Финал"
    );
    private static final int THREAD_POOL = 6;

    private final FilmManagerService filmManagerService = SpringUtility.getBean(FilmManagerService.class);
    private final FilmRepository filmRepository = SpringUtility.getBean(FilmRepository.class);
    private final ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL);

    private int count = 0;

    @Override
    public void migrate(Context context) {
        filmList.forEach(s -> executor.execute(() -> insertFilm(s)));
    }

    private void insertFilm(String filmName) {
        if (!filmRepository.existsByFilmName(filmName)) {
            filmManagerService.parseFilm(filmName);
        }
        done();
    }

    private synchronized void done() {
        count += 1;
        if (count == filmList.size()) {
            executor.shutdownNow();
        }
    }

}
