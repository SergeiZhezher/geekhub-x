package com.libfilm.service;

import java.util.UUID;
import java.util.regex.Pattern;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.libfilm.dao.FilmEntity;
import com.libfilm.dao.RatingEntity;
import com.libfilm.dao.UserEntity;
import com.libfilm.exception.InvalidInputDataException;
import com.libfilm.repository.FilmRepository;
import com.libfilm.repository.RatingRepository;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class FilmPageService {

    private static final Pattern FLOAT_PATTERN = Pattern.compile("[+-]?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?");

    private final RatingRepository ratingRepository;
    private final FilmRepository filmRepository;

    public FilmPageService(final RatingRepository ratingRepository, final FilmRepository filmRepository) {
        this.ratingRepository = ratingRepository;
        this.filmRepository = filmRepository;
    }

    public FilmEntity getFilmInfoById(final UUID id) {
        return filmRepository.findById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    @Transactional
    public ResponseEntity<Void> updateRating(final UserEntity user, final UUID filmId, final String rating) {
        FilmEntity film = filmRepository.
                findById(filmId).orElseThrow(() -> new InvalidInputDataException("Invalid filmId"));

        if (ratingRepository.existsByAuthorAndFilm(user, film)) {
            throw new InvalidInputDataException("You have already rated");
        }
        film.setVotes(film.getVotes() + 1);
        film.setRating(film.getRating() + getValidRating(rating));

        filmRepository.save(film);
        ratingRepository.save(new RatingEntity(user, film));

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private float getValidRating(String rating) {
        boolean isFloat = rating != null && FLOAT_PATTERN.matcher(rating).matches();

        if (!isFloat) {
            throw new InvalidInputDataException("Invalid rating");
        }
        float acceptedRating = Float.parseFloat(rating);

        if (acceptedRating > 10 || acceptedRating <= 0) {
            throw new InvalidInputDataException("Invalid rating");
        }

        return acceptedRating;
    }

}
