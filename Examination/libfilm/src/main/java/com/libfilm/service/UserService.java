package com.libfilm.service;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import com.libfilm.dao.Role;
import com.libfilm.dao.UserEntity;
import com.libfilm.exception.ForbiddenException;
import com.libfilm.exception.InvalidInputDataException;
import com.libfilm.exception.RegisteredUserNotAutoAuthorizedException;
import com.libfilm.exception.UserAlreadyExistsException;
import com.libfilm.repository.UserRepository;

import static org.springframework.http.HttpStatus.CREATED;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final CommentService commentService;
    private final PasswordEncoder bCryptPasswordEncoder;

    public UserService(
            final UserRepository userRepository,
            final CommentService commentService,
            final PasswordEncoder bCryptPasswordEncoder
    ) {
        this.userRepository = userRepository;
        this.commentService = commentService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<UserEntity> getUsers() {
        List<UserEntity> users = userRepository.findAll();

        if (users.isEmpty()) {
            throw new EntityNotFoundException();
        }

        return users;
    }

    public ResponseEntity<Void> addUser(
            final UserEntity user,
            final BindingResult errors,
            final HttpServletRequest request
    ) {
        String email = user.getEmail();
        String password = user.getPassword();
        String confirmedPassword = user.getConfirmedPassword();

        saveUser(errors, email, password, confirmedPassword);
        try {
            request.login(email, password);
        } catch (ServletException e) {
            throw new RegisteredUserNotAutoAuthorizedException("Registered user wasn't auto authorized: " + email);
        }

        return new ResponseEntity<>(CREATED);
    }

    public ResponseEntity<Void> addUserManual(final UserEntity user, final BindingResult errors) {
        String email = user.getEmail();
        String password = user.getPassword();
        String confirmedPassword = user.getConfirmedPassword();

        saveUser(errors, email, password, confirmedPassword);

        return new ResponseEntity<>(CREATED);
    }

    private void saveUser(
            final BindingResult errors,
            final String email,
            final String password,
            final String confirmedPassword
    ) {
        if (errors.hasErrors()) {
            throw new InvalidInputDataException(errors.getAllErrors().get(0).getDefaultMessage());
        }
        if (!password.equals(confirmedPassword)) {
            throw new InvalidInputDataException("Password can be equals!");
        }
        if (userRepository.existsByEmail(email)) {
            throw new UserAlreadyExistsException("Such user already exists!");
        }
        userRepository.save(new UserEntity(email, bCryptPasswordEncoder.encode(password),
                Collections.singleton(Role.USER), true));
    }

    public ResponseEntity<Void> updateUser(
            final UserEntity user, final BindingResult errors, final UserEntity updatableUser
    ) {
        if (errors.hasErrors()) {
            throw new InvalidInputDataException(errors.getAllErrors().get(0).getDefaultMessage());
        }
        if (!user.getPassword().equals(user.getConfirmedPassword())) {
            throw new InvalidInputDataException("Password can be equals!");
        }
        if (updatableUser.getRoles().contains(Role.SUPER_ADMIN)) {
            throw new ForbiddenException();
        }
        updatableUser.setEmail(user.getEmail());
        updatableUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        userRepository.save(updatableUser);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Void> updateRole(final String role, final UserEntity user, final UserEntity current) {
        if (user.getId().equals(current.getId()) || !current.getRoles().contains(Role.SUPER_ADMIN)) {
            throw new ForbiddenException();
        }
        if (role.equals(Role.SUPER_ADMIN.name())) {
            throw new ForbiddenException();
        }
        user.getRoles().clear();
        user.getRoles().add(Role.valueOf(role));

        userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Void> updateActive(final Boolean active, final UserEntity user, final UserEntity current) {
        if (user.getId().equals(current.getId()) || user.getRoles().contains(Role.SUPER_ADMIN)) {
            throw new ForbiddenException();
        }
        user.setActive(active);
        userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Transactional
    public ResponseEntity<Void> deleteUser(final UserEntity user, final UserEntity current) {
        if (user.getId().equals(current.getId()) || user.getRoles().contains(Role.SUPER_ADMIN)) {
            throw new ForbiddenException();
        }
        commentService.deleteCommentsByUser(user);
        userRepository.deleteById(user.getId());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
