package com.libfilm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.libfilm.dao.CommentEntity;
import com.libfilm.dao.UserEntity;
import com.libfilm.exception.InvalidCommentException;
import com.libfilm.repository.CommentRepository;
import com.libfilm.repository.FilmRepository;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

@Service
public class CommentService {

    public static final Pattern UUID_PATTERN = Pattern.compile("([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})");

    private final CommentRepository commentRepository;
    private final FilmRepository filmRepository;

    public CommentService(CommentRepository commentRepository, FilmRepository filmRepository) {
        this.commentRepository = commentRepository;
        this.filmRepository = filmRepository;
    }

    public CommentEntity addComment(String data, UserEntity user) {
        Map<String, String> jsonMap = convertJsonStringToMap(data);

        String comment = jsonMap.get("comment");
        String filmId = jsonMap.get("filmId");

        if (!isValidComment(comment, filmId) || !filmRepository.existsById(UUID.fromString(filmId))) {
            throw new InvalidCommentException("Comment data is invalid");
        }
        CommentEntity entity = new CommentEntity(UUID.fromString(filmId), comment, LocalDateTime.now(), user);

        commentRepository.save(entity);

        return entity;
    }

    private Map<String, String> convertJsonStringToMap(String json) {
        try {
            return new ObjectMapper().readValue(json, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            throw new InvalidCommentException("Received comment JSON is invalid!");
        }
    }

    private boolean isValidComment(String comment, String filmId) {
        return comment != null && filmId != null
                && !comment.isBlank()
                && comment.length() <= 1024
                && UUID_PATTERN.matcher(filmId).matches();
    }

    public List<CommentEntity> getCommentsByFilmId(UUID id) {
        return commentRepository.
                findTop20AllByFilmId(id, Sort.by(Sort.Direction.DESC, "time"))
                .orElse(Collections.emptyList());
    }

    public void deleteCommentsByUser(UserEntity user) {
        commentRepository.deleteAllByAuthor(user);
    }

}
