package com.libfilm.service;

import com.libfilm.dao.FilmEntity;
import com.libfilm.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class FilmService {

    public static final Pattern INTEGER_PATTERN = Pattern.compile("^-?\\d+$");
    private final FilmRepository filmRepository;

    public FilmService(final FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    public Page<FilmEntity> getFilmsForMainPage(
            final Pageable pageable,
            final String yearMin,
            final String yearMax
    ) {
        return getFilmList(filmRepository.findAll(pageable), yearMin, yearMax, null);
    }

    public Page<FilmEntity> getFilmsByGenre(
            final Pageable pageable,
            final String yearMin,
            final String yearMax,
            final String genre
    ) {
        return getFilmList(filmRepository.findAllByGenre(pageable, genre), yearMin, yearMax, genre);
    }

    public Page<FilmEntity> getFilmList(
            final Page<FilmEntity> films,
            final String yearMin,
            final String yearMax,
            final String genre
    ) {
        Page<FilmEntity> filmList = films;

        if (isValidYearFilters(yearMin, yearMax)) {
            Pageable pageable = PageRequest.of(0, 8, Sort.Direction.DESC, "years");

            if (genre != null) {
                filmList = filmRepository
                        .findAllByYearsGreaterThanEqualAndYearsLessThanEqualAndGenre(
                                pageable, Integer.parseInt(yearMin), Integer.parseInt(yearMax), genre
                        );
            } else {
                filmList = filmRepository
                        .findAllByYearsGreaterThanEqualAndYearsLessThanEqual(
                                pageable, Integer.parseInt(yearMin), Integer.parseInt(yearMax)
                        );
            }
        }
        if (filmList != null) {
            return filmList;
        }

        return Page.empty();
    }

    public List<String> getFilmGenres() {
        return filmRepository.fetchFilmGenre().orElseGet(Collections::emptyList);
    }

    public List<FilmEntity> getHints(final String filmName) {
        return filmRepository
                .findTop5AllByFilmNameContainingIgnoreCase(filmName)
                .orElseGet(Collections::emptyList);
    }

    public boolean isValidYearFilters(final String yearMin, final String yearMax) {
        return yearMin != null && yearMax != null
                && INTEGER_PATTERN.matcher(yearMin).matches()
                && INTEGER_PATTERN.matcher(yearMax).matches();
    }

}
