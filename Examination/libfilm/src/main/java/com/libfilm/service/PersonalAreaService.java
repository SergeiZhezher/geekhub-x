package com.libfilm.service;

import com.libfilm.dao.UserEntity;
import com.libfilm.exception.InvalidInputDataException;
import com.libfilm.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PersonalAreaService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public PersonalAreaService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public ResponseEntity<Void> updatePassword(
            final UserEntity user,
            final String currentPassword,
            final String newPassword,
            final String confirmedPassword
    ) {
        if (newPassword.trim().length() < 3 || newPassword.trim().length() > 128) {
            throw new InvalidInputDataException("Password size can be between 3-128");
        }
        if (!newPassword.equals(confirmedPassword)) {
            throw new InvalidInputDataException("Passwords must be equals!");
        }
        if (!passwordEncoder.matches(currentPassword, user.getPassword())) {
            throw new InvalidInputDataException("Wrong current password!");
        }
        user.setPassword(passwordEncoder.encode(newPassword));

        userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
