package com.libfilm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.libfilm.dao.FilmEntity;
import com.libfilm.exception.InvalidInputDataException;
import com.libfilm.exception.parser.FilmAlreadyExistException;
import com.libfilm.repository.FilmRepository;
import com.libfilm.util.parser.FilmParserFactory;
import com.libfilm.util.parser.FilmParserName;
import com.libfilm.util.parser.FilmParserStatus;
import com.libfilm.util.parser.abstraction.FilmParser;
import com.libfilm.util.validator.FilmManagerValidator;

@Service
public class FilmManagerService {

    private final FilmManagerValidator filmManagerValidator;
    private final FilmParserFactory filmParserFactory;
    private final FilmRepository filmRepository;

    public FilmManagerService(
            final FilmManagerValidator filmManagerValidator,
            final FilmParserFactory filmParserFactory,
            final FilmRepository filmRepository
    ) {
        this.filmManagerValidator = filmManagerValidator;
        this.filmParserFactory = filmParserFactory;
        this.filmRepository = filmRepository;
    }

    public String parseFilm(final String filmName) {
        if (!filmManagerValidator.isValidFilmName(filmName)) {
            throw new InvalidInputDataException("The name cannot have size less than 3 or contain only a space");
        }
        if (filmRepository.existsByFilmName(filmName)) {
            throw new FilmAlreadyExistException("This film has already been parsed: " + filmName);
        }
        FilmParser filmParser = filmParserFactory.getParser(FilmParserName.HDREZKA);

        String filmLink = filmParser.getFilmLinkByNameFromSearch(filmName);

        FilmEntity filmData = filmParser.getFilmDataByLink(filmLink);
        filmData.setFilmName(filmName);

        filmRepository.save(filmData);

        return FilmParserStatus.SUCCESS.name();
    }

    public Map<String, List<String>> getHints(final String filmName) {
        FilmParser filmParser = filmParserFactory.getParser(FilmParserName.HDREZKA);

        List<String> hintsLibFilm;
        List<String> hintsHDREZKA;

        hintsHDREZKA = filmParser.parseHints(filmName);
        hintsLibFilm = filmRepository.fetchFilmNamesByPrefix(filmName);

        return getCombinedHints(hintsLibFilm, hintsHDREZKA);
    }

    public void deleteFilm(final String filmName, final HttpServletResponse response) {
        if (filmManagerValidator.isValidFilmName(filmName)) {
            filmRepository.deleteByFilmName(filmName);

            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private Map<String, List<String>> getCombinedHints(
            final List<String> hintsLibFilm,
            final List<String> hintsHDREZKA
    ) {
        Map<String, List<String>> hints = new HashMap<>();

        if (!hintsLibFilm.isEmpty()) {
            hints.put("LibFilm:", hintsLibFilm);
        }
        if (!hintsHDREZKA.isEmpty()) {
            hints.put("HDREZKA:", hintsHDREZKA);
        }

        return hints;
    }

}
