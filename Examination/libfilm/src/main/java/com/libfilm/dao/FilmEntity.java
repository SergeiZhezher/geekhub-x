package com.libfilm.dao;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "film")
public class FilmEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(nullable = false, unique = true)
    private String filmName;

    @Column(length = 2048)
    private String description;

    @Column(nullable = false)
    private String trailerLink;

    @Column(nullable = false)
    private String duration;

    @Column(nullable = false)
    private String genre;

    @Column(nullable = false)
    private Integer years;

    @Column(nullable = false)
    private Float rating;

    @Column(nullable = false)
    private Float votes;

    @Column(nullable = false)
    private String posterUrl;

    public FilmEntity() {
    }

    public FilmEntity(String filmName, String description, String trailerLink, String duration, String genre, Integer years, Float rating, Float votes, String posterUrl) {
        this.filmName = filmName;
        this.description = description;
        this.trailerLink = trailerLink;
        this.duration = duration;
        this.genre = genre;
        this.years = years;
        this.rating = rating;
        this.votes = votes;
        this.posterUrl = posterUrl;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTrailerLink() {
        return trailerLink;
    }

    public void setTrailerLink(String trailerLink) {
        this.trailerLink = trailerLink;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getYears() {
        return years;
    }

    public void setYears(Integer years) {
        this.years = years;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Float getVotes() {
        return votes;
    }

    public void setVotes(Float votes) {
        this.votes = votes;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FilmEntity that = (FilmEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(filmName, that.filmName) && Objects.equals(description, that.description) && Objects.equals(trailerLink, that.trailerLink) && Objects.equals(duration, that.duration) && Objects.equals(genre, that.genre) && Objects.equals(years, that.years) && Objects.equals(rating, that.rating) && Objects.equals(votes, that.votes) && Objects.equals(posterUrl, that.posterUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, filmName, description, trailerLink, duration, genre, years, rating, votes, posterUrl);
    }

    @Override
    public String toString() {
        return "FilmEntity{" +
                "id=" + id +
                ", filmName='" + filmName + '\'' +
                ", description='" + description + '\'' +
                ", trailerLink='" + trailerLink + '\'' +
                ", duration='" + duration + '\'' +
                ", genre='" + genre + '\'' +
                ", years=" + years +
                ", rating=" + rating +
                ", votes=" + votes +
                ", posterUrl=" + posterUrl +
                '}';
    }
}
