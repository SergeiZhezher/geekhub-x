package com.libfilm.exception.parser;

public class UnknownFilmParserException extends RuntimeException {

    public UnknownFilmParserException(String message) {
        super(message);
    }

}
