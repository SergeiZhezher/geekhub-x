package com.libfilm.exception;

public class InvalidCommentException extends IllegalArgumentException {

    public InvalidCommentException(String message) {
        super(message);
    }

}
