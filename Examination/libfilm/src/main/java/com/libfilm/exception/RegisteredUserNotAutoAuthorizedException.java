package com.libfilm.exception;

public class RegisteredUserNotAutoAuthorizedException extends RuntimeException {

    public RegisteredUserNotAutoAuthorizedException(String message) {
        super(message);
    }

}
