package com.libfilm.exception.parser;

public class PostFetcherException extends RuntimeException {

    public PostFetcherException(String message) {
        super(message);
    }

}
