package com.libfilm.exception.parser;

public class FilmDataWasNotSaveException extends RuntimeException {

    public FilmDataWasNotSaveException(String message) {
        super(message);
    }

}
