package com.libfilm.exception.parser;

public class FilmParserException extends RuntimeException {

    public FilmParserException(String message) {
        super(message);
    }

}
