package com.libfilm.config;

import com.libfilm.dao.Role;
import com.libfilm.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl userDetailsService;
    private final int managementPort;

    public WebSecurityConfig(
            final UserDetailsServiceImpl userDetailsService
//            final @Value("${management.server.port}") int managementPort
    ) {
        this.userDetailsService = userDetailsService;
        this.managementPort = 5001;
    }

    @Bean
    protected PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                authorizeRequests().
                antMatchers("/", "/css/**", "/js/**", "/img/**", "/webjars/**", "/registration").permitAll().
                antMatchers("/actuator/**").hasAnyAuthority(Role.SUPER_ADMIN.name()).
                requestMatchers(
                        matcherByPortAndPath(managementPort, HttpMethod.GET, "/**"),
                        matcherByPortAndPath(managementPort, HttpMethod.POST, "/**"),
                        matcherByPortAndPath(managementPort, HttpMethod.PATCH, "/**"),
                        matcherByPortAndPath(managementPort, HttpMethod.PUT, "/**"),
                        matcherByPortAndPath(managementPort, HttpMethod.DELETE, "/**")
                ).denyAll().
                anyRequest().authenticated().

                and().
                formLogin().
                loginPage("/login").permitAll().
                defaultSuccessUrl("/").
                failureUrl("/?error").

                and().
                sessionManagement().
                invalidSessionUrl("/").

                and().
                rememberMe().rememberMeParameter("remember-me").tokenValiditySeconds(60 * 60 * 24 * 7).

                and().
                logout().
                logoutRequestMatcher(new AntPathRequestMatcher("/logout", HttpMethod.POST.name())).
                invalidateHttpSession(true).
                clearAuthentication(true).
                deleteCookies("JSESSIONID");
    }

    private RequestMatcher matcherByPortAndPath(final int port, final HttpMethod method, final String pathPattern) {
        return new AndRequestMatcher(matcherByPort(port), new AntPathRequestMatcher(pathPattern, method.name()));
    }

    private RequestMatcher matcherByPort(final int port) {
        return (HttpServletRequest request) -> port == request.getLocalPort();
    }

}
