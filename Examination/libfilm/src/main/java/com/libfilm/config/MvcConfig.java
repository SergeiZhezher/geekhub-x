package com.libfilm.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/manager/users").setViewName("user-manager");
        registry.addViewController("/me").setViewName("personal-area");
        registry.addViewController("/film/*").setViewName("film-page");
        registry.addViewController("/genre/*").setViewName("main");
        registry.addViewController("/").setViewName("main");
    }

}
