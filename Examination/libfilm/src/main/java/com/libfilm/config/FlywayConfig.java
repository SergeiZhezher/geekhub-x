package com.libfilm.config;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.sql.DataSource;

@Configuration
public class FlywayConfig {

    @Bean
    @DependsOn("springUtility")
    public Flyway flywayPostgres(DataSource dataSource) {

        return Flyway.configure()
                .ignoreFutureMigrations(true)
                .ignoreMissingMigrations(true)
                .outOfOrder(true)
                .dataSource(dataSource)
                .load();
    }

    @Bean
    public InitializingBean flywayMigratePostgres(Flyway flyway) {
        return flyway::migrate;
    }

}
