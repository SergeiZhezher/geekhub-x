package com.libfilm.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:db.properties")
public class DataSourceConfig {

    @Profile("prod")
    @Bean
    @ConfigurationProperties("prod.datasource")
    public DataSource devDataSource(
            final @Value("${prod.datasource.url}") String url,
            final @Value("${prod.datasource.username}") String username,
            final @Value("${prod.datasource.password}") String password
    ) {
        final HikariConfig config = new HikariConfig();

        config.setDriverClassName("org.postgresql.Driver");

        return getDataSource(url, username, password, config);
    }

    @Profile("dev")
    @Bean
    @ConfigurationProperties("dev.datasource")
    public DataSource prodDataSource(
            final @Value("${dev.datasource.url}") String url,
            final @Value("${dev.datasource.username}") String username,
            final @Value("${dev.datasource.password}") String password
    ) {
        final HikariConfig config = new HikariConfig();

        config.setDriverClassName("org.h2.Driver");

        return getDataSource(url, username, password, config);
    }

    private DataSource getDataSource(
            final String url,
            final String username,
            final String password,
            final HikariConfig config
    ) {
        config.setJdbcUrl(url);

        config.setUsername(username);
        config.setPassword(password);

        config.setMaximumPoolSize(25);
        config.setMinimumIdle(1);
        config.setConnectionTimeout(15_000);
        config.setMaxLifetime(60_000);

        return new HikariDataSource(config);
    }

}

