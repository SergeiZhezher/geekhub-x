package com.libfilm.repository;

import com.libfilm.dao.FilmEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface FilmRepository extends JpaRepository<FilmEntity, UUID> {

    Page<FilmEntity> findAll(Pageable pageable);

    Page<FilmEntity> findAllByGenre(Pageable pageable, String genre);

    Page<FilmEntity> findAllByYearsGreaterThanEqualAndYearsLessThanEqual(Pageable pageable, int min, int max);

    Page<FilmEntity> findAllByYearsGreaterThanEqualAndYearsLessThanEqualAndGenre(Pageable pageable, int min, int max, String genre);

    Optional<List<FilmEntity>> findTop5AllByFilmNameContainingIgnoreCase(String filmName);

    boolean existsById(UUID id);
    boolean existsByFilmName(String name);

    @Transactional
    void deleteByFilmName(String filmName);

    @Query(value = "SELECT film_name FROM film WHERE film_name ILIKE %:filmName% LIMIT 3", nativeQuery = true)
    List<String> fetchFilmNamesByPrefix(@Param("filmName") String filmName);

    @Query(value = "SELECT DISTINCT genre FROM film", nativeQuery = true)
    Optional<List<String>> fetchFilmGenre();
}
