package com.libfilm.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.libfilm.dao.CommentEntity;
import com.libfilm.dao.UserEntity;

@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, UUID> {

    Optional<List<CommentEntity>> findTop20AllByFilmId(UUID id, Sort orders);

    @Transactional
    void deleteAllByAuthor(UserEntity user);

}
