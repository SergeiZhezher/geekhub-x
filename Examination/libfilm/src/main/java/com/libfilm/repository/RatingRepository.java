package com.libfilm.repository;

import com.libfilm.dao.FilmEntity;
import com.libfilm.dao.RatingEntity;
import com.libfilm.dao.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RatingRepository extends JpaRepository<RatingEntity, UUID> {

    boolean existsByAuthorAndFilm(UserEntity userEntity, FilmEntity film);

}
