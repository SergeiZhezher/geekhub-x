package com.libfilm.util.parser.hdrezka;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.asynchttpclient.Param;
import org.asynchttpclient.util.HttpConstants;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.libfilm.dao.FilmEntity;
import com.libfilm.exception.parser.FilmNotFoundException;
import com.libfilm.exception.parser.FilmParserException;
import com.libfilm.util.parser.FilmParserStatus;
import com.libfilm.util.parser.PostFetcherImpl;
import com.libfilm.util.parser.abstraction.FilmParser;

import static org.springframework.http.HttpHeaders.ACCEPT_ENCODING;
import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpHeaders.REFERER;
import static org.springframework.http.HttpHeaders.USER_AGENT;

@Component
public class FilmParserHDREZKA implements FilmParser {

    private static final Map<? extends CharSequence, ? extends Iterable<?>> headers = Map.of(
            ACCEPT_ENCODING, List.of("gzip, deflate, br"),
            ACCEPT_LANGUAGE, List.of("ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"),
            CONTENT_TYPE, List.of("application/x-www-form-urlencoded; charset=UTF-8"),
            REFERER, List.of("https://rezka.ag/"),
            USER_AGENT, List.of("Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Mobile Safari/537.36")
    );

    private final PostFetcherImpl postFetcherImpl;

    public FilmParserHDREZKA(final PostFetcherImpl postFetcherImpl) {
        this.postFetcherImpl = postFetcherImpl;
    }

    @Override
    public String getFilmLinkByNameFromSearch(final String filmName) {
        String searchName = filmName.replaceAll("\\s", "+");
        String searchUrl = String.format("https://rezka.ag/search/?do=search&subaction=search&q=%s", searchName);

        String filmLink;
        String foundName;
        try {
            Element searchContainer = getDocumentByUrl(searchUrl)
                    .getElementsByClass("b-content__inline_items").get(0);

            if (searchContainer.childrenSize() == 0) {
                throw new FilmNotFoundException(FilmParserStatus.FILM_NOT_FOUND.name());
            }
            foundName = searchContainer.getElementsByClass("b-content__inline_item-link")
                    .get(0).select("a[href^=https://rezka.ag/]").text();

            if (!foundName.equalsIgnoreCase(filmName)) {
                throw new FilmNotFoundException(FilmParserStatus.FILM_NOT_FOUND.name());
            }
            filmLink = searchContainer.getElementsByClass("b-content__inline_item").attr("data-url");

        } catch (IOException | IllegalArgumentException e) {
            throw new FilmParserException("Parsing link error");
        }

        return filmLink;
    }

    @Override
    public FilmEntity getFilmDataByLink(final String linkFilm) {
        FilmEntity filmEntity = new FilmEntity();

        try {
            Document doc = getDocumentByUrl(linkFilm);

            String filmGenre = doc.select("span[itemprop^=genre]").text();
            String filmDuration = doc.select("td[itemprop^=duration]").text();
            String filmDescription = doc.getElementsByClass("b-post__description_text").text();
            String filmYear = doc.select("a[href^=https://rezka.ag/year/]").text();
            String rating = doc.getElementsByClass("b-post__info_rates kp").get(0).child(1).text();
            String votes = doc.getElementsByClass("b-post__info_rates kp").get(0).child(2).text();
            String dataId = doc.getElementsByClass("b-sidelinks__link show-trailer").attr("data-id");

            filmEntity.setVotes(Float.parseFloat(votes.replaceAll("\\D", "")));
            filmEntity.setRating(Float.parseFloat(String.format("%.1f", Float.parseFloat(rating))
                    .replace(',', '.')) * filmEntity.getVotes());
            filmEntity.setDuration(filmDuration);
            filmEntity.setDescription(filmDescription);
            filmEntity.setYears(Integer.parseInt(filmYear.replaceAll("\\D", "")));
            filmEntity.setGenre(filmGenre.split(" ")[0]);
            filmEntity.setPosterUrl(doc.select("img[itemprop^=image]").attr("src"));
            filmEntity.setTrailerLink(getFilmTrailerLink(dataId));

        } catch (Exception e) {
            throw new FilmParserException("Parsing film data error");
        }

        return filmEntity;
    }

    @Override
    public List<String> parseHints(final String filmName) {
        List<String> hints = new ArrayList<>();

        String html = postFetcherImpl.fetch(headers, List.of(new Param("q", filmName)),
                HttpConstants.Methods.POST, "https://rezka.ag/engine/ajax/search.php");

        Document doc = Jsoup.parse(html);
        Elements elements = doc.getElementsByClass("enty");

        elements.forEach(element -> hints.add(element.text()));

        return hints;
    }

    private String getFilmTrailerLink(final String dataId) throws JsonProcessingException {
        String result = postFetcherImpl.fetch(headers, List.of(new Param("id", dataId)),
                HttpConstants.Methods.POST, "https://rezka.ag/engine/ajax/gettrailervideo.php");

        String html = (String) new ObjectMapper().readValue(result, Map.class).get("code");

        return parseAttributeValueBySelector(html, "iframe[src^=https://www.youtube.com/]", "src");
    }

    private String parseAttributeValueBySelector(final String html, final String selector, final String attributeName) {
        Document doc = Jsoup.parse(html);
        Element element = doc.select(selector).get(0);

        return element.attr(attributeName);
    }

    private Document getDocumentByUrl(final String searchUrl) throws IOException {
        return Jsoup.connect(searchUrl)
                .timeout(25000)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36")
                .referrer("https://www.google.com")
                .get();
    }

}