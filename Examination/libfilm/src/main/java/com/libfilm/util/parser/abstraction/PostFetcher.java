package com.libfilm.util.parser.abstraction;

import java.util.List;
import java.util.Map;

import org.asynchttpclient.Param;

public interface PostFetcher {

    String fetch(Map<? extends CharSequence, ? extends Iterable<?>> headers,
                 List<Param> params,
                 String httpMethod,
                 String url);

}
