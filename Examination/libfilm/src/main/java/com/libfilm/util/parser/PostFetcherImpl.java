package com.libfilm.util.parser;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.Param;
import org.asynchttpclient.RequestBuilder;
import org.asynchttpclient.Response;
import org.springframework.stereotype.Component;

import com.libfilm.exception.parser.PostFetcherException;
import com.libfilm.util.parser.abstraction.PostFetcher;

@Component
public class PostFetcherImpl implements PostFetcher {

    private final AsyncHttpClient client = Dsl.asyncHttpClient();

    @Override
    public String fetch(
            final Map<? extends CharSequence, ? extends Iterable<?>> headers,
            final List<Param> params,
            final String httpMethod,
            final String url
    ) {
        var getRequest = new RequestBuilder(httpMethod)
                .setHeaders(headers)
                .setFormParams(params)
                .setUrl(url)
                .build();

        Future<Response> responseFuture = client.executeRequest(getRequest);

        return getResponse(responseFuture).getResponseBody();
    }

    private Response getResponse(Future<Response> responseFuture) {
        Response response;

        try {
            response = responseFuture.get();
        } catch (ExecutionException e) {
            throw new PostFetcherException(e.getMessage());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new PostFetcherException(e.getMessage());
        }
        if (response.getStatusCode() > 299) {
            throw new PostFetcherException("Fetch was not success");
        }

        return response;
    }

}
