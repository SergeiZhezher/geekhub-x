package com.libfilm.util.parser;

public enum FilmParserStatus {
    FILM_NOT_FOUND,
    FILM_PARSER_ERROR,
    FILM_DATA_WAS_NOT_SAVE,
    FILM_ALREADY_EXIST,
    SUCCESS
}
