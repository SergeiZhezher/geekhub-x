package com.libfilm.util.parser.abstraction;

import java.util.List;

import com.libfilm.dao.FilmEntity;

public interface FilmParser {

    String getFilmLinkByNameFromSearch(final String filmName);

    FilmEntity getFilmDataByLink(final String linkFilm);

    List<String> parseHints(final String filmName);

}
