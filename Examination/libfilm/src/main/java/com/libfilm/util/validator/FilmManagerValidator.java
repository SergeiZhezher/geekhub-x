package com.libfilm.util.validator;

import org.springframework.stereotype.Component;

@Component
public class FilmManagerValidator {

    public boolean isValidFilmName(final String filmName) {
        return filmName.length() > 2 && !filmName.isBlank();
    }

}
