package com.libfilm.util.parser;

import org.springframework.stereotype.Component;

import com.libfilm.exception.parser.UnknownFilmParserException;
import com.libfilm.util.parser.abstraction.FilmParser;
import com.libfilm.util.parser.hdrezka.FilmParserHDREZKA;

@Component
public class FilmParserFactory {

    private final FilmParserHDREZKA filmParserHDREZKA;

    public FilmParserFactory(FilmParserHDREZKA filmParserHDREZKA) {
        this.filmParserHDREZKA = filmParserHDREZKA;
    }

    public FilmParser getParser(FilmParserName parserName) {

        if (parserName.equals(FilmParserName.HDREZKA)) {
            return filmParserHDREZKA;
        }

        throw new UnknownFilmParserException("Unknown parser: " + parserName);
    }

}
