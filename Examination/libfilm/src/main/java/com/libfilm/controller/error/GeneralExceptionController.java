package com.libfilm.controller.error;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.libfilm.exception.BadRequestException;
import com.libfilm.exception.ForbiddenException;
import com.libfilm.exception.InvalidInputDataException;
import com.libfilm.exception.RegisteredUserNotAutoAuthorizedException;
import com.libfilm.exception.UserAlreadyExistsException;

@ControllerAdvice
public class GeneralExceptionController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = InvalidInputDataException.class)
    public ResponseEntity<String> invalidInputData(InvalidInputDataException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<String> entityNotFound(EntityNotFoundException exception) {
        return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = UserAlreadyExistsException.class)
    public ResponseEntity<String> userAlreadyExists(UserAlreadyExistsException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = RegisteredUserNotAutoAuthorizedException.class)
    public ResponseEntity<String> registeredUserNotAuthorized(RegisteredUserNotAutoAuthorizedException exception) {
        logger.error("User wasn't auto authorized", exception);
        return new ResponseEntity<>("User wasn't auto authorized", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = ForbiddenException.class)
    public ResponseEntity<Void> forbidden(ForbiddenException exception) {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<String> badRequest(BadRequestException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
