package com.libfilm.controller.rest;

import com.libfilm.service.FilmManagerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@PreAuthorize("hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
public class FilmManagerRestController {

    private final FilmManagerService filmManagerService;

    public FilmManagerRestController(final FilmManagerService filmManagerService) {
        this.filmManagerService = filmManagerService;
    }

    @PostMapping("/manager/films")
    public ResponseEntity<String> parseFilm(final @RequestParam(name = "filmName") String filmName) {
        return new ResponseEntity<>(filmManagerService.parseFilm(filmName), HttpStatus.CREATED);
    }

    @PostMapping("/manager/films/hints")
    public ResponseEntity<Map<String, List<String>>> getHints(final @RequestParam(name = "filmName") String filmName) {
        return new ResponseEntity<>(filmManagerService.getHints(filmName), HttpStatus.OK);
    }

    @DeleteMapping("/manager/film")
    public void deleteFilm(
            final @RequestParam(name = "filmName") String filmName,
            final HttpServletResponse response
    ) {
        filmManagerService.deleteFilm(filmName, response);
    }

}
