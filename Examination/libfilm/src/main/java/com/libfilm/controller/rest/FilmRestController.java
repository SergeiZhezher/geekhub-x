package com.libfilm.controller.rest;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.libfilm.dao.FilmEntity;
import com.libfilm.service.FilmService;

@RestController
@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'SUPER_ADMIN')")
public class FilmRestController {

    private static final int NUMBER_FILM_ON_PAGE = 8;
    private static final String RATTING_FIELD = "rating";
    private static final String YEAR_MIN_PARAM = "yearMin";
    private static final String YEAR_MAX_PARAM = "yearMax";

    private final FilmService filmService;

    public FilmRestController(final FilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping("/films")
    public ResponseEntity<Page<FilmEntity>> mainPage(
            final @RequestParam(name = RATTING_FIELD, required = false) String rating,
            final @RequestParam(name = YEAR_MIN_PARAM, required = false) String yearMin,
            final @RequestParam(name = YEAR_MAX_PARAM, required = false) String yearMax,
            @PageableDefault(sort = {"id"},
                    direction = Sort.Direction.DESC, size = NUMBER_FILM_ON_PAGE) Pageable pageable
    ) {
        if (rating != null) {
            pageable = PageRequest.of(0, NUMBER_FILM_ON_PAGE, Sort.Direction.DESC, RATTING_FIELD);
        }
        Page<FilmEntity> filmsForMainPage = filmService.getFilmsForMainPage(pageable, yearMin, yearMax);

        return new ResponseEntity<>(filmsForMainPage, HttpStatus.OK);
    }

    @GetMapping("/films/genre/{genre}")
    public ResponseEntity<Page<FilmEntity>> genrePage(
            final @PathVariable String genre,
            final @RequestParam(name = RATTING_FIELD, required = false) String rating,
            final @RequestParam(name = YEAR_MIN_PARAM, required = false) String yearMin,
            final @RequestParam(name = YEAR_MAX_PARAM, required = false) String yearMax,
            @PageableDefault(sort = {"id"},
                    direction = Sort.Direction.DESC, size = NUMBER_FILM_ON_PAGE) Pageable pageable
    ) {
        if (rating != null) {
            pageable = PageRequest.of(0, NUMBER_FILM_ON_PAGE, Sort.Direction.DESC, RATTING_FIELD);
        }
        Page<FilmEntity> filmsByGenre = filmService.getFilmsByGenre(pageable, yearMin, yearMax, genre);

        return new ResponseEntity<>(filmsByGenre, HttpStatus.OK);
    }

    @GetMapping("/films/genres")
    public ResponseEntity<List<String>> getGenres() {
        return new ResponseEntity<>(filmService.getFilmGenres(), HttpStatus.OK);
    }

    @GetMapping("/films/hints")
    public ResponseEntity<List<FilmEntity>> getHints(final @RequestParam(name = "filmName") String filmName) {
        return new ResponseEntity<>(filmService.getHints(filmName), HttpStatus.OK);
    }

}

