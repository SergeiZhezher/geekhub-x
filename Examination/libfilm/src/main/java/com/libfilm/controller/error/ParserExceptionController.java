package com.libfilm.controller.error;

import com.libfilm.exception.parser.FilmAlreadyExistException;
import com.libfilm.exception.parser.FilmDataWasNotSaveException;
import com.libfilm.exception.parser.FilmNotFoundException;
import com.libfilm.exception.parser.FilmParserException;
import com.libfilm.exception.parser.PostFetcherException;
import com.libfilm.util.parser.FilmParserStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ParserExceptionController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = PostFetcherException.class)
    public ResponseEntity<String> postFetch(PostFetcherException exception) {
        logger.error("Post fetch was not success", exception);

        return new ResponseEntity<>(FilmParserStatus.FILM_PARSER_ERROR.name(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = FilmNotFoundException.class)
    public ResponseEntity<String> filmNotFound(FilmNotFoundException exception) {
        logger.warn(exception.getMessage(), exception);

        return new ResponseEntity<>(FilmParserStatus.FILM_NOT_FOUND.name(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = FilmParserException.class)
    public ResponseEntity<String> filmParserError(FilmParserException exception) {
        logger.error(exception.getMessage(), exception);

        return new ResponseEntity<>(FilmParserStatus.FILM_PARSER_ERROR.name(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = FilmDataWasNotSaveException.class)
    public ResponseEntity<String> filmDataWasNotSave(FilmDataWasNotSaveException exception) {
        logger.error(exception.getMessage(), exception);

        return new ResponseEntity<>(FilmParserStatus.FILM_DATA_WAS_NOT_SAVE.name(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = FilmAlreadyExistException.class)
    public ResponseEntity<String> filmAlreadyExist(FilmAlreadyExistException exception) {
        logger.info(exception.getMessage(), exception);

        return new ResponseEntity<>(FilmParserStatus.FILM_ALREADY_EXIST.name(), HttpStatus.BAD_REQUEST);
    }

}
