package com.libfilm.controller.rest;

import com.libfilm.dao.UserEntity;
import com.libfilm.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class RegistrationRestController {

    private final UserService userService;

    public RegistrationRestController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registration")
    public ResponseEntity<Void> addUser(
            final @Valid UserEntity user,
            final BindingResult bindingResult,
            final HttpServletRequest httpServletRequest
    ) {
        return userService.addUser(user, bindingResult, httpServletRequest);
    }

}
