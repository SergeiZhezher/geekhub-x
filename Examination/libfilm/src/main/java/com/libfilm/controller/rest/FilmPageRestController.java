package com.libfilm.controller.rest;

import com.libfilm.dao.FilmEntity;
import com.libfilm.dao.UserEntity;
import com.libfilm.service.FilmPageService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'SUPER_ADMIN')")
public class FilmPageRestController {

    private final FilmPageService filmPageService;

    public FilmPageRestController(final FilmPageService filmPageService) {
        this.filmPageService = filmPageService;
    }

    @GetMapping("/film/id/{id}")
    public ResponseEntity<FilmEntity> filmPage(final @PathVariable() UUID id) {
        return new ResponseEntity<>(filmPageService.getFilmInfoById(id), HttpStatus.OK);
    }

    @PatchMapping("/film/rating")
    public ResponseEntity<Void> updateRating(
            final @AuthenticationPrincipal UserEntity user,
            final @RequestParam(name = "rating") String rating,
            final @RequestParam(name = "filmId") UUID filmId
    ) {
        return filmPageService.updateRating(user, filmId, rating);
    }

}
