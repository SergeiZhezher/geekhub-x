package com.libfilm.controller.rest;

import com.libfilm.dao.Role;
import com.libfilm.dao.UserEntity;
import com.libfilm.exception.BadRequestException;
import com.libfilm.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@PreAuthorize("hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
public class UserManagerRestController {

    private final UserService userService;

    public UserManagerRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserEntity>> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<Void> addUser(final @Valid UserEntity user, final BindingResult bindingResult) {
        return userService.addUserManual(user, bindingResult);
    }

    @PutMapping("/users")
    public ResponseEntity<Void> updateUser(
            final @Valid UserEntity user,
            final BindingResult bindingResult,
            final @RequestParam(name = "currentId") UserEntity updatableUser
    ) {
        return userService.updateUser(user, bindingResult, updatableUser);
    }

    @PatchMapping("/users")
    public ResponseEntity<Void> updateData(
            final @AuthenticationPrincipal UserEntity current,
            final @RequestParam(name = "id") UserEntity usr,
            final @RequestParam(name = "role", required = false) String role,
            final @RequestParam(name = "active", required = false) Boolean active
    ) {
        if (active != null) {
            return userService.updateActive(active, usr, current);
        }
        if (role != null) {
            return Stream.of(Role.values()).map(Enum::name).collect(Collectors.toList()).contains(role) ?
                    userService.updateRole(role, usr, current) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        throw new BadRequestException("Role or active must be specified!");
    }

    @DeleteMapping("/users")
    public ResponseEntity<Void> deleteUser(
            final @RequestParam(name = "id") UserEntity usr,
            final @AuthenticationPrincipal UserEntity current
    ) {
        return userService.deleteUser(usr, current);
    }

}
