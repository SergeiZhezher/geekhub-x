package com.libfilm.controller.rest;

import com.libfilm.dao.CommentEntity;
import com.libfilm.dao.UserEntity;
import com.libfilm.exception.InvalidCommentException;
import com.libfilm.service.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class CommentRestController {

    private final SimpMessagingTemplate commentSender;
    private final CommentService commentService;

    public CommentRestController(final SimpMessagingTemplate commentSender, final CommentService commentService) {
        this.commentSender = commentSender;
        this.commentService = commentService;
    }

    @GetMapping("/film/{id}/comments")
    public ResponseEntity<List<CommentEntity>> getComments(final @PathVariable UUID id) {
        return new ResponseEntity<>(commentService.getCommentsByFilmId(id), HttpStatus.OK);
    }

    @MessageMapping("/comment")
    public void getComment(final @RequestParam(name = "data") String data, final Authentication authentication) {
        UserEntity user = (UserEntity) authentication.getPrincipal();

        CommentEntity entity = commentService.addComment(data, user);
        commentSender.convertAndSend("/comment/film/" + entity.getFilmId(), entity);
    }

    @MessageExceptionHandler
    private void handleException(final InvalidCommentException ex, final Authentication authentication) {
        UserEntity user = (UserEntity) authentication.getPrincipal();

        commentSender.convertAndSend("/comment/film/error/" + user.getId(), ex.getMessage());
    }

}
