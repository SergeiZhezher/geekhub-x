package com.libfilm.controller.view;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@PreAuthorize("hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
public class FilmManagerController {

    @GetMapping("/manager/films")
    public String filmManager() {
        return "film-manager";
    }

}
