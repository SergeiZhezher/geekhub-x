package com.libfilm.controller.rest;

import com.libfilm.dao.UserEntity;
import com.libfilm.service.PersonalAreaService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'SUPER_ADMIN')")
public class PersonalAreaRestController {

    private final PersonalAreaService personalAreaService;

    public PersonalAreaRestController(final PersonalAreaService personalAreaService) {
        this.personalAreaService = personalAreaService;
    }

    @PatchMapping("/personal-area")
    public ResponseEntity<Void> updatePassword(
            final @AuthenticationPrincipal UserEntity user,
            final @RequestParam(name = "currentPassword") String currentPassword,
            final @RequestParam(name = "newPassword") String newPassword,
            final @RequestParam(name = "confirmedPassword") String confirmedPassword
    ) {
        return personalAreaService.updatePassword(user, currentPassword, newPassword, confirmedPassword);
    }

}
