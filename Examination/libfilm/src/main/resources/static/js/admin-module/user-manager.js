let token = $('#_csrf').attr('content');
let header = $('#_csrf_header').attr('content');

function create_user() {
    let name = prompt("Name: ");
    let password = prompt("Password: ");
    let confirmed_password = prompt("Confirm password: ");

    $.ajax({
        type: "POST",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            name: name,
            password: password,
            confirmedPassword: confirmed_password
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 201) {
                reload_page();
            }
        },
        error: function (data, textStatus, xhr) {
            alert(data.responseText)
        }
    });
}

function load_users() {
    $.ajax({
        type: "GET",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                for (let user of data) {
                    let checked_status = '';
                    if (user.active === true) {
                        checked_status = 'checked';
                    }

                    $("#users-container").append(
                        '<tr data-id="' + user.id + '"><td>' + user.name + '</td>' +
                        '<td> ' +
                        '<select onchange="update_role(this);" id="test">' +
                        '<option value="' + user.id + '">USER</option>' +
                        '<option value="' + user.id + '">ADMIN</option>' +
                        '<option disabled selected="selected">' + user.roles + '</option>' +
                        '</select>  ' +
                        '</td>' +
                        '<td><input type="checkbox" data-id="' + user.id + '"' + checked_status + ' onclick="update_active(this)"></td>' +
                        '<td><button data-id="' + user.id + '" onclick="update_user(this)">Full update</button></td>' +
                        '<td><button data-id="' + user.id + '" onclick="delete_user(this)">remove</button></td></tr>'
                    );

                }
            }
        }
    });
}

function update_role(val) {
    let option = val.options[val.selectedIndex];

    $.ajax({
        type: "PATCH",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            id: option.value,
            role: option.text,
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                console.log("Success update")
                reload_page();
            }
        }
    });
}

function delete_user(elm) {
    let id = $(elm).attr("data-id");

    $.ajax({
        type: "DELETE",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            id: id
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                $('tr[data-id="' + id + '"]').remove();
                console.log("Success delete")
            }
        }
    });
}

function update_user(elm) {
    let id = $(elm).attr("data-id");

    let name = prompt("New name: ");
    let password = prompt("New Password: ");
    let confirmed_password = prompt("Confirmed new Password: ");

    $.ajax({
        type: "PUT",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            name: name,
            password: password,
            confirmedPassword: confirmed_password,
            currentId: id
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                reload_page();
            }
        },
        error: function (data, textStatus, xhr) {
            alert(data.responseText)
        }
    });
}

function update_active(elm) {
    let id = $(elm).attr("data-id");
    let active = $(elm).is(':checked');

    $.ajax({
        type: "PATCH",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            id: id,
            active: active
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                console.log("Success update active status");
                reload_page();
            }
        }
    });
}

function reload_page() {
    setTimeout(function () {
        document.location.reload();
    }, 200);
}