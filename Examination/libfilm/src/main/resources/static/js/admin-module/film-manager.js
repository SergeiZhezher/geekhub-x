let token = $('#_csrf').attr('content');
let header = $('#_csrf_header').attr('content');

let prefixLibFilm = 'LibFilm:';
let prefixHDREZKA = 'HDREZKA:';

function hintHandler() {
    let res = $(this).text();
    res = res.replace(prefixLibFilm, '');
    res = res.replace(prefixHDREZKA, '');

    res = res.trim();
    $(".name").val(res);
}

$('.name').keyup(function () {
    let film_name = $('.name').val();

    if (film_name.length >= 2) {
        $.ajax({
            type: "POST",
            url: "/manager/films/hints",
            beforeSend: function (xhr) {
                xhr.setRequestHeader(header, token);
            },
            data: {
                filmName: film_name
            },
            success: function (data, textStatus, xhr) {
                $('#hint-container div').remove();
                for (const [prefix, film_names] of Object.entries(data)) {
                    for (let film_name of film_names) {
                        $('#hint-container').append('<div class="hint" onclick="hintHandler.apply(this)">' + prefix + ' ' + film_name + '</div>');
                    }
                }
            },
            error: function (data) {
                alert(data.responseText)
            }
        });
    } else {
        $('#hint-container div').remove();
    }

});

function parse_film() {
    let film_name = $(".name").val();

    if (check_film_name(film_name)) {
        $.ajax({
            type: "POST",
            url: "/manager/films",
            beforeSend: function (xhr) {
                xhr.setRequestHeader(header, token);
            },
            data: {
                filmName: film_name
            },
            success: function (data, textStatus, xhr) {
                alert(data);
            },
            error: function (data) {
                alert(data.responseText)
            }
        });
    }

}

function delete_film() {
    let film_name = $(".name").val();

    if (check_film_name(film_name)) {
        $.ajax({
            url: '/manager/film',
            type: 'DELETE',
            beforeSend: function (xhr) {
                xhr.setRequestHeader(header, token);
            },
            data: {
                filmName: film_name
            }
        })
    }
}

function check_film_name(film_name) {
    if (film_name.length > 2 && film_name.trim()) {
        $(".name").val('');
        $('#hint-container div').remove();

        return true;
    } else {
        alert("The name cannot have size less than 3 or contain only a space ");
        $(".name").val('');
        $('#hint-container div').remove();

        return false;
    }
}