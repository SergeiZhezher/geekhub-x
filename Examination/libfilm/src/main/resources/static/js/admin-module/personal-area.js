function change_password() {

    let currentPassword = prompt("Please enter current password");
    let newPassword = prompt("Please enter new password");
    let confirmedPassword = prompt("Please confirm new password");

    if (is_valid_passwords(currentPassword, newPassword, confirmedPassword)) {
        save_new_password(currentPassword, newPassword, confirmedPassword)
    }
}

function is_valid_passwords(currentPassword, newPassword, confirmedPassword) {
    if (currentPassword.trim().length < 3 || newPassword.trim().length < 3) {
        alert("Passwords cannot be less than 3 characters long !")
        return false;
    }
    if (currentPassword === newPassword) {
        alert("The current password cannot be equals new password !")
        return false;
    }
    if (newPassword !== confirmedPassword) {
        alert("Passwords must be equals !")
        return false;
    }

    return true;
}

function save_new_password(currentPassword, newPassword, confirmedPassword) {
    let token = $('#_csrf').attr('content');
    let header = $('#_csrf_header').attr('content');

    $.ajax({
        type: "PATCH",
        url: "/personal-area",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            currentPassword: currentPassword,
            newPassword: newPassword,
            confirmedPassword: confirmedPassword
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                alert("Success")
                reload_page();
            }
        },
        error: function (data, textStatus, xhr) {
            alert(data.responseText)
        }
    });
}

function shutdown() {
    let header = $('#_csrf_header').attr('content');
    let token = $('#_csrf').attr('content');

    $.ajax({
        type: "post",
        url: "/actuator/shutdown",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        success: function (data, textStatus, xhr) {
        }
    })
}