let socket = new SockJS('/comment-end-point');
stompClient = Stomp.over(socket);
stompClient.debug = null

let userId = $('#user_id').attr('content');
let film_id = '';

$(document).ready(function () {
    load_genres();
    load_film();

    connect()
})

function load_film() {
    let href = window.location.href;
    let film_key_word = 'film/';

    $.ajax({
        type: "GET",
        url: '/film/id/' + href.substr(href.indexOf(film_key_word) + film_key_word.length),
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $(".name").text(data.filmName);
                $("#video-player").attr("src", data.trailerLink);
                set_rating(data.rating / data.votes / 2)

                film_id = data.id;
                load_comments();
            }
        }
    });
}

function load_comments() {
    $.ajax({
        type: "GET",
        url: '/film/' + film_id + '/comments/',
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                for (let datum of data) {
                    display_comment(datum.comment, datum.author.email, datum.time, false)
                }
            }
        }
    });
}

function display_comment(comment, email, time, isUp) {
    let html =
        '<article class="msg-container msg-remote" id="msg-0">' +
        '<div class="msg-box">' +
        '<img class="userEntity-img" id="userEntity-0" src="//gravatar.com/avatar/00034587632094500000000000000000?d=retro"/>' +
        '<div class="flr">' +
        '<div class="messages">' +
        '<p class="msg" id="msg-0">' + comment + '</p>' +
        '</div>' +
        '<span class="timestamp"><span class="username">' + email + '</span>&bull;' +
        '<span class="posttime">' + time + '</span>' +
        '</span></div></div></article>';

    if (isUp) {
        $('.chat-window').prepend(html);
        return;
    }
    $('.chat-window').append(html);
}

function connect() {
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/comment/film/' + film_id, function (response) {
            let data = JSON.parse(response.body);
            display_comment(data.comment, data.author.email, data.time, true)
        });

        stompClient.subscribe('/comment/film/error/' + userId, function (response) {
            alert(response.body)
        });
    });
}

function sendMessage() {
    let comment = $("#comment-field").val();
    $('#comment-field').val('');

    stompClient.send("/libfilm/comment", {}, JSON.stringify({
        'comment': comment,
        'filmId': film_id
    }));
}

window.addEventListener("unload", function () {
    stompClient.disconnect();
});

function set_rating(rating) {
    $(".my-rating-6").starRating({
        totalStars: 5,
        emptyColor: 'lightgray',
        hoverColor: 'slategray',
        activeColor: 'cornflowerblue',
        initialRating: rating,
        strokeWidth: 0,
        useGradient: false,
        minRating: 1,
        readOnly: false,

        callback: function (currentRating, $el) {
            update_rating(currentRating);
        }
    });
}

function update_rating(rating) {
    let current_header = $('#_csrf_header').attr('content');
    let current_token = $('#_csrf').attr('content');

    $.ajax({
        type: "PATCH",
        url: "/film/rating",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(current_header, current_token);
        },
        data: {
            rating: rating,
            filmId: film_id
        },
        success: function (data, textStatus, xhr) {
        },
        error: function (data, textStatus, xhr) {
            alert(data.responseText)
        }
    })
}