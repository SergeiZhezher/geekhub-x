$(document).ready(function () {
    let href = window.location.href;
    let genre_key_word = 'genre/';

    if (href.includes(genre_key_word)) {
        load_films('/films/genre/' + href.substr(href.indexOf(genre_key_word) + genre_key_word.length));
    } else {
        load_films('/films/');
    }

    load_genres();
});

function load_films(url) {
    $.ajax({
        type: "GET",
        url: url + window.location.search,
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200 && xhr.getResponseHeader("Content-Type") === 'application/json') {
                $('.item').remove();
                for (let film of data.content) {
                    $('#content-container').append(
                        '<div class="item"">' +
                        '<img src="' + film.posterUrl +'" height="70%" width="35%" class="poster">' +
                        '<input class="movie-watch-button" data-id="' + film.id + '" type="button" value="Watch now" onclick="go_to_the_movie_page(this)">' +
                        '<p class="name">' + film.filmName + '</p>' +
                        '<p class="genre">Genre: ' + film.genre + '</p>' +
                        '<p class="year">Year: ' + film.years + '</p>' +
                        '<p class="duration">duration: ' + film.duration + '</p>' +
                        '<div class="my-rating-6"></div>' +
                        '        <details class="description">' +
                        '            <summary>Description</summary>' +
                        '            <p>' + film.description + '</p>' +
                        '        </details>' +
                        '        <script>' +
                        '            $(".my-rating-6").starRating({' +
                        '                initialRating: ' + film.rating / film.votes / 2 + ',' +
                        '                useGradient: false,' +
                        '                readOnly: true' +
                        '            });' +
                        '        </script>' +
                        '</div>'
                    )
                }

                create_pagination(data.totalPages)
            }
        }
    });
}

function go_to_the_movie_page(elm) {
    let id = $(elm).attr("data-id");
    window.location = window.location.protocol + '//' + window.location.host + '/film/' + id;
}

function create_pagination(total_pages) {
    $('#pagination-demo').twbsPagination({
        totalPages: total_pages,
        visiblePages: 6,
        next: 'Next',
        prev: 'Prev',
        initiateStartPageClick: false,
        pageVariable: 1,
        onPageClick: function (event, page) {
            window.location = '/?page=' + (page - 1);
        }
    });
    fix_pagination_buttons()
}

function fix_pagination_buttons() {
    $('.pagination-sm li').removeClass("active");
    $('.pagination-sm li').removeClass("disabled");
}