let token = $('#_csrf').attr('content');
let header = $('#_csrf_header').attr('content');

$(document).ready(function () {
    $('.menu-tab').click(function () {
        $('.menu-hide').toggleClass('show');
        $('.menu-tab').toggleClass('show');
    });
});

function handle_filters(elm) {
    let href = window.location.href;
    let genre_key_word = 'genre/';

    if (href.includes(genre_key_word)) {
        load_films('/films/' + href.substr(href.indexOf(genre_key_word)) + '' + elm.value);
    } else {
        load_films('/films/' + elm.value);
    }
}

function handleSelect(elm) {
    window.location = window.location.protocol + '//' + window.location.host + '/' + elm.value;
}

function load_genres() {
    $.ajax({
            type: "GET",
            url: "/films/genres",
            success: function (data, textStatus, xhr) {
                if (xhr.status === 200 && xhr.getResponseHeader("Content-Type") === 'application/json') {
                    for (let genre of data) {
                        $('.menu-hide').append(
                            '<div> <a href="/genre/' + genre + '">' +
                            '<div>' + genre + '</div>' +
                            '</a>' +
                            '</div>'
                        );
                    }
                }
            }
        }
    )
}

function registration() {
    let email_pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    $('#registration_message').empty();

    let email = $('#reg_email').val();
    let password = $('#reg_password').val();
    let confirmed_password = $('#reg_confirmed_password').val();

    if (!email_pattern.test(email) || email.trim().length < 3) {
        $("#registration_message").text("Email is invalid");
        return;
    }
    if (password !== confirmed_password || password.trim().length < 3) {
        $("#registration_message").text("Passwords is invalid");
        return;
    }

    $.ajax({
        type: "post",
        url: "/registration",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            email: email,
            password: password,
            confirmedPassword: confirmed_password
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 201) {
                document.location.reload();
            }
        },
        error: function (data, textStatus, xhr) {
            $("#registration_message").text(data.responseText);
        }
    })
}