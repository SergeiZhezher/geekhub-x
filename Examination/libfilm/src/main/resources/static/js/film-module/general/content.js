let is_login_shown = false;

$('#login_button').click(function () {
    if (is_login_shown === false) {
        $('.login-html').show(250);
        is_login_shown = true
    } else {
        $('.login-html').hide(250);
        is_login_shown = false
    }
});


$('#search').keyup(function () {
    let film_name = $('#search').val();

    if (film_name.length >= 2) {
        $.ajax({
            type: "GET",
            url: "/films/hints",
            data: {
                filmName: film_name
            },
            success: function (data, textStatus, xhr) {
                $('#hint-container a').remove();

                if (xhr.status === 200 && xhr.getResponseHeader("Content-Type") === 'application/json') {
                    for (let i = 0; i < data.length; i++) {
                        $('#hint-container').append('<a href="/film/' + data[i].id + '"><div class="hint">' + data[i].filmName + '</div></a>');
                    }
                }
            }
        });
    } else {
        $('#hint-container a').remove();
    }
});
