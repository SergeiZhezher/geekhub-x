create table comment
(
    id      uuid          not null,
    comment varchar(1024) not null,
    film_id uuid          not null,
    time    timestamp     not null,
    user_id uuid,
    primary key (id)
);