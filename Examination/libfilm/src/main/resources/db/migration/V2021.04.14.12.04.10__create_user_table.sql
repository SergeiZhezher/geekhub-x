create table usr
(
    id        uuid         not null,
    email     varchar(255) not null unique,
    is_active boolean default true,
    password  varchar(128) not null,
    primary key (id)
);