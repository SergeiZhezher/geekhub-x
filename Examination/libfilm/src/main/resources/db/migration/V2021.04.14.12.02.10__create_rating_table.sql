create table rating
(
    id      uuid not null,
    user_id uuid,
    film_id uuid unique,
    primary key (id)
);