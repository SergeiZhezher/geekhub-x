-- SUPER_ADMIN@gmail.com : SUPER_ADMIN
insert into usr (id, email, password, is_active)
values ('4e38099a-83f1-11eb-8dcd-0242ac130003', 'SUPER_ADMIN@gmail.com', '$2y$12$f9Skfky/IJbM5AoeuoVpmOKIOJSG4O0wsZJUft45DrVhbkf0/vf/2', true);

insert into user_role (user_id, roles)
values ('4e38099a-83f1-11eb-8dcd-0242ac130003', 'SUPER_ADMIN');