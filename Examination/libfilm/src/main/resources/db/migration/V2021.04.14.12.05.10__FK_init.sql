alter table comment
    add constraint FK_comment_to_user foreign key (user_id) references usr;

alter table rating
    add constraint FK_rating_to_user foreign key (user_id) references usr;

alter table rating
    add constraint FK_rating_to_film foreign key (film_id) references film;

alter table user_role
    add constraint FK_role_to_user foreign key (user_id) references usr;