create table film
(
    id           uuid         not null,
    description  varchar(2048),
    duration     varchar(255) not null,
    film_name    varchar(255) not null unique,
    genre        varchar(255) not null,
    poster_url   varchar(255) not null,
    rating       float        not null,
    trailer_link varchar(255) not null,
    votes        float        not null,
    years        integer      not null,
    primary key (id)
);