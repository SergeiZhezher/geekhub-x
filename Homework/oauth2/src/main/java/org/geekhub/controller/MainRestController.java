package org.geekhub.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/users")
public class MainRestController {

    @GetMapping("/me")
    public Map<String, Object> currentUser(@AuthenticationPrincipal OAuth2User user) {
        return Map.of(
                "username", Objects.requireNonNull(user.getAttribute("name")),
                "avatar_url", Objects.requireNonNull(user.getAttribute("avatar_url"))
        );
    }

}