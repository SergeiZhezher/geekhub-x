package org.geekhub.student.service.abstractions;

import org.geekhub.student.dao.Student;

import java.util.Collections;
import java.util.List;

public interface StudentSource {
    default List<Student> getStudentList(int totalStudentsCount) {
        return Collections.emptyList();
    }
}
