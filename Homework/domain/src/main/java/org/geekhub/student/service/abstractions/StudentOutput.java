package org.geekhub.student.service.abstractions;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;

import java.util.List;
import java.util.Map;

public interface StudentOutput {
    default void printTableList(Map<GradeType, List<Student>> students) {}
    default void printTableList(List<Student> students) {}
}
