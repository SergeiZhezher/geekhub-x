package org.geekhub.student.dao;

import org.geekhub.student.grades.service.Grade;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Student implements Serializable {

    private static final long serialVersionUID = Long.MAX_VALUE;

    private String name;
    private Grade grade;
    private LocalDateTime gradeTime;

    public Student(String name, Grade grade, LocalDateTime gradeTime) {
        this.name = name;
        this.grade = grade;
        this.gradeTime = gradeTime;
    }

    public String getName() {
        return name;
    }

    public Grade getGrade() {
        return grade;
    }

    public String getGradeAsString() {
        return grade.asPrintVersion();
    }

    public LocalDateTime getGradeTime() {
        return gradeTime;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", grade=" + grade +
                ", gradeTime=" + gradeTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name) &&
                Objects.equals(grade, student.grade) &&
                Objects.equals(gradeTime, student.gradeTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, grade, gradeTime);
    }
}