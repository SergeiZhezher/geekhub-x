package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StudentSorter {

    public Map<GradeType, List<Student>> getSortedListByGrade(Map<GradeType, List<Student>> students) {
        Map<GradeType, List<Student>> result = new HashMap(students);
        result.forEach((g, l) -> {
            if (g.equals(GradeType.GPA)) {
                result.replace(GradeType.GPA, getSortedListByGpaOrUkrainian(students.get(GradeType.GPA)));
            } else if (g.equals(GradeType.LETTER)) {
                result.replace(GradeType.LETTER, getSortedListByLetter(students.get(GradeType.LETTER)));
            } else if (g.equals(GradeType.PERCENTAGE)) {
                result.replace(GradeType.PERCENTAGE, getSortedListByPercentage(students.get(GradeType.PERCENTAGE)));
            } else if (g.equals(GradeType.UKRAINIAN)) {
                result.replace(GradeType.UKRAINIAN, getSortedListByGpaOrUkrainian(students.get(GradeType.UKRAINIAN)));
            } else {
                throw new IllegalArgumentException();
            }
        });
        return result;
    }

    public List<Student> getSortedListByName(List<Student> studentList) {
        List<Student> result = new ArrayList<>(studentList);
        result.sort(Comparator.comparing(Student::getName, String.CASE_INSENSITIVE_ORDER));
        return result;
    }

    private List<Student> getSortedListByGpaOrUkrainian(List<Student> students) {
        List<Student> result = new ArrayList<>(students);
        result.sort(Comparator.comparing((Student s) -> Float.parseFloat(s.getGradeAsString())).reversed());
        return result;
    }

    private List<Student> getSortedListByLetter(List<Student> students) {
        List<Student> result = new ArrayList<>(students);
        result.sort(Comparator.comparing(Student::getGradeAsString));
        return result;
    }

    private List<Student> getSortedListByPercentage(List<Student> students) {
        List<Student> result = new ArrayList<>(students);
        result.sort(Comparator.comparing((Student student) -> student.getGradeAsString()
                .replaceAll("[^0-9]", "")).reversed());
        return result;
    }
}
