package org.geekhub.student.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:db.properties")
public class SpringJdbcConfig {

    @Bean(name = "postgres")
    @ConditionalOnProperty(value = "datasource.storage-type", havingValue = "PG")
    public DataSource dataSourcePostgres(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password
    ) {
        final HikariConfig config = new HikariConfig();

        config.setDriverClassName("org.postgresql.Driver");

        return getDataSource(url, username, password, config);
    }

    @Bean(name = "jdbcPostgres")
    @ConditionalOnProperty(value = "datasource.storage-type", havingValue = "PG")
    public JdbcTemplate jdbcTemplatePostgres(@Qualifier("postgres") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "h2")
    @ConditionalOnProperty(value = "datasource.storage-type", havingValue = "H2")
    public DataSource dataSourceH2(
            @Value("${spring.h2-datasource.url}") String url,
            @Value("${spring.h2-datasource.username}") String username,
            @Value("${spring.h2-datasource.password}") String password
    ) {
        final HikariConfig config = new HikariConfig();

        config.setDriverClassName("org.h2.Driver");

        return getDataSource(url, username, password, config);
    }

    @Bean(name = "jdbcH2")
    @ConditionalOnProperty(value = "datasource.storage-type", havingValue = "H2")
    public JdbcTemplate jdbcTemplateH2(@Qualifier("h2") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    private DataSource getDataSource(String url, String username, String password, HikariConfig config) {
        config.setJdbcUrl(url);

        config.setUsername(username);
        config.setPassword(password);

        config.setMaximumPoolSize(25);
        config.setMinimumIdle(1);
        config.setConnectionTimeout(15_000);
        config.setMaxLifetime(60_000);

        return new HikariDataSource(config);
    }

}

