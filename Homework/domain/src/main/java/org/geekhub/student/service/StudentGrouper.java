package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.types.Gpa;
import org.geekhub.student.grades.types.Letter;
import org.geekhub.student.grades.types.Percentage;
import org.geekhub.student.grades.types.Ukrainian;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class StudentGrouper {

    public Map<GradeType, List<Student>> getGroupedStudents(List<Student> studentList) {
        List<Student> studentGpa = new ArrayList<>();
        List<Student> studentLetter = new ArrayList<>();
        List<Student> studentPercentage = new ArrayList<>();
        List<Student> studentUkrainian = new ArrayList<>();

        for (Student s : studentList) {
            if (s.getGrade() instanceof Gpa) {
                studentGpa.add(s);
            } else if (s.getGrade() instanceof Letter) {
                studentLetter.add(s);
            } else if (s.getGrade() instanceof Percentage) {
                studentPercentage.add(s);
            } else if (s.getGrade() instanceof Ukrainian) {
                studentUkrainian.add(s);
            }
        }

        return Map.of(
                GradeType.GPA, studentGpa, GradeType.LETTER, studentLetter,
                GradeType.PERCENTAGE, studentPercentage, GradeType.UKRAINIAN, studentUkrainian
        );
    }

}
