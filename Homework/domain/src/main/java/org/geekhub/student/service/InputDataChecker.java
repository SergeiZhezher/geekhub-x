package org.geekhub.student.service;

import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.service.GradeFactoryInitialization;
import org.geekhub.student.util.exceptions.GradeNotSupportedException;
import org.geekhub.student.util.exceptions.InvalidInputException;
import org.geekhub.student.util.exceptions.InvalidScoreException;
import org.geekhub.student.util.logger.Logger;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

@Component
public class InputDataChecker {

    private final Logger logger = new Logger(InputDataChecker.class.getName());

    public boolean isValidInputData(String name, String type, String value, String date) {

        try {
            isNotEmpty(new String[]{name, type, value, date});
            getExistType(type);
            getExistGrade(getIntegerNumber(value));
            getValidDateTime(date);

        } catch (IllegalArgumentException e) {
            logger.warning(e);
            return false;
        }
        return true;
    }

    private void isNotEmpty(String[] strings) {
        for (String string : strings) {
            if (string.isBlank()) {
                throw new IllegalArgumentException("Input field is empty");
            }
        }
    }

    public String getExistType(String type) {
        try {
            Enum.valueOf(GradeType.class, type);
            return type;
        } catch (IllegalArgumentException e) {
            throw new GradeNotSupportedException(type + " unsupported data (symbol/word) passed as a grade type");
        }
    }

    public int getExistGrade(int value) {
        if (value > 100 || value < 0) {
            throw new InvalidScoreException("Entered score value is not in allowed range");
        } else {
            return value;
        }
    }

    public int getIntegerNumber(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new InvalidInputException("Entered not expected data");
        }
    }

    public Grade getGrade(String gradeType, int gradeValue) {
        GradeFactoryInitialization factoryInitialization = new GradeFactoryInitialization();
        return factoryInitialization.createGrade(GradeType.valueOf(gradeType), gradeValue);
    }

    public LocalDateTime getValidDateTime(String date) {
        LocalDateTime localDateTime;

        try {
            localDateTime = LocalDateTime.parse(date);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Input date-time is invalid");
        }

        return localDateTime;
    }
}
