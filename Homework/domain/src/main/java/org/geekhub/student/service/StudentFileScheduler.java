package org.geekhub.student.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.service.GradeFactoryInitialization;
import org.geekhub.student.service.abstractions.StudentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class StudentFileScheduler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final GradeFactoryInitialization gradeFactoryInitialization;
    private final StudentManager studentManager;

    public StudentFileScheduler(GradeFactoryInitialization gradeFactoryInitialization, StudentManager studentManager) {
        this.gradeFactoryInitialization = gradeFactoryInitialization;
        this.studentManager = studentManager;
    }

    @Scheduled(cron = "@daily")
    public void extractStudents() {
        try {
            List<Student> students = convertJsonToStudentList(readStudents());
            studentManager.writeStudentsIfNotExist(students);
        } catch (Exception e) {
            logger.warn("Students have not been read from a file in the home directory", e);
        }
    }

    private List<Map<String, String>> readStudents() throws IOException {
        Path filePath = Path.of(System.getProperty("user.home")).resolve("students.txt");
        String json = Files.readString(filePath);

        return new ObjectMapper().readValue(json, new TypeReference<>() {
        });
    }

    private List<Student> convertJsonToStudentList(List<Map<String, String>> json) {
        List<Student> students = new ArrayList<>();

        for (Map<String, String> jsonMap : json) {
            Grade grade = gradeFactoryInitialization.
                    createGrade(GradeType.valueOf(jsonMap.get("grade")), Integer.parseInt(jsonMap.get("gradeValue")));

            students.add(new Student(jsonMap.get("name"), grade, LocalDateTime.parse(jsonMap.get("gradeTime"))));
        }

        return students;
    }

}
