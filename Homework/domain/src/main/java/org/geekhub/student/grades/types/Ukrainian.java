package org.geekhub.student.grades.types;

import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.util.exceptions.ScoreNotSupportedException;

public class Ukrainian implements Grade {

    int value;

    public Ukrainian(int value) {
        this.value = value;
    }

    @Override
    public String asPrintVersion() {

        if (value < 5 && value >= 0) {
            return "1";
        } else if (value < 10 && value >= 5) {
            return "2";
        } else if (value >= 10 && value <= 100) {
            return String.valueOf(value / 10 + 2);
        }

        throw new ScoreNotSupportedException("Score value not supported: " + value);
    }

    @Override
    public int getValue() {
        return value;
    }

}
