package org.geekhub.student.util.cache;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class StudentAnalyticsCacheProvider {

    private static final long CACHE_DELETION_INTERVAL = 5L;

    private final ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();

    private List<String> cachedAnalytics = Collections.synchronizedList(new ArrayList<>());

    public List<String> getCachedAnalytics() {
        return cachedAnalytics;
    }

    public void setCachedAnalytics(List<String> cachedAnalytics) {
        this.cachedAnalytics = cachedAnalytics;
    }

    public void deleteCachedAnalytics() {
        cachedAnalytics.clear();
    }

    @PostConstruct
    private void runAutomaticCacheDeletion() {
        timer.scheduleWithFixedDelay(
                this::deleteCachedAnalytics, CACHE_DELETION_INTERVAL, CACHE_DELETION_INTERVAL, TimeUnit.MINUTES);
    }

    @PreDestroy
    private void stopAutomaticCacheDeletion() {
        timer.shutdownNow();
    }

}
