package org.geekhub.student.config.flyway;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ConditionalOnProperty(value = "datasource.storage-type", havingValue = "PG")
public class FlywayPgConfig {

    @Bean(name = "flywayPostgres")
    public Flyway flywayPostgres(@Qualifier("postgres") DataSource dataSource) {

        return Flyway.configure()
                .ignoreFutureMigrations(true)
                .ignoreMissingMigrations(true)
                .outOfOrder(true)
                .locations("classpath:db/migration/postgres")
                .dataSource(dataSource)
                .load();
    }

    @Bean
    public InitializingBean flywayMigratePostgres(@Qualifier("flywayPostgres") Flyway flyway) {
        return flyway::migrate;
    }

}
