package org.geekhub.student.service.analytics;

import java.util.Optional;

public interface StudentAnalytics {
    Optional<Integer> averageScore(String type);

    Optional<Integer> maxScore(String type);

    Optional<Integer> minScore(String type);

    Optional<Integer> medianScore(String type);
}
