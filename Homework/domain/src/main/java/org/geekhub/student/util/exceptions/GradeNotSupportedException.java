package org.geekhub.student.util.exceptions;

public class GradeNotSupportedException extends InvalidInputException {
    public GradeNotSupportedException(String message) {
        super(message);
    }
    public GradeNotSupportedException() {
        super("is unknown grade");
    }
}
