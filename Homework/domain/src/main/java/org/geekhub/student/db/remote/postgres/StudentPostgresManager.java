package org.geekhub.student.db.remote.postgres;

import org.geekhub.student.dao.Student;
import org.geekhub.student.db.remote.DbManager;
import org.geekhub.student.service.abstractions.StudentManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@ConditionalOnProperty(value = "datasource.storage-type", havingValue = "PG")
public class StudentPostgresManager implements StudentManager {

    private final String avgGradeValueForPG;
    private final String medianGradeValueForPG;

    private final DbManager dbManager;
    private final JdbcTemplate jdbcTemplate;

    public StudentPostgresManager(
            DbManager dbManager,
            @Qualifier("jdbcPostgres") JdbcTemplate jdbcTemplate,
            @Value("${avgGradeValueForPG}") String avgGradeValueForPG,
            @Value("${medianGradeValueForPG}") String medianGradeValueForPG
    ) {
        this.jdbcTemplate = jdbcTemplate;
        this.dbManager = dbManager;
        this.avgGradeValueForPG = avgGradeValueForPG;
        this.medianGradeValueForPG = medianGradeValueForPG;
    }

    @Override
    public List<Student> readStudents() {
        return dbManager.readStudents(jdbcTemplate);
    }

    @Override
    public void writeStudents(List<Student> generated, List<Student> read) {
        List<Student> students = new ArrayList<>(generated);

        dbManager.write(students, jdbcTemplate);
    }

    @Override
    public void deleteAllStudents() {
        dbManager.delete(jdbcTemplate);
    }

    @Override
    public Optional<Integer> getMaxGradeValueByGradeType(String type) {
        int max = dbManager.getMaxGradeValueByGradeType(type, jdbcTemplate);

        return max == 0 ? Optional.empty() : Optional.of(max);
    }

    @Override
    public Optional<Integer> getMinGradeValueByGradeType(String type) {
        int min = dbManager.getMinGradeValueByGradeType(type, jdbcTemplate);

        return min == 0 ? Optional.empty() : Optional.of(min);
    }

    @Override
    public Optional<Integer> getAvgGradeValueByGradeType(String type) {
        return Optional.ofNullable(jdbcTemplate.queryForObject(
                avgGradeValueForPG, Integer.class, type));
    }

    @Override
    public Optional<Integer> getMedianGradeValueByGradeType(String type) {
        return Optional.ofNullable(jdbcTemplate.queryForObject(
                medianGradeValueForPG, Integer.class, type));
    }

    @Override
    public void writeStudentsIfNotExist(List<Student> students) {
        dbManager.writeIfNotExist(students, jdbcTemplate);
    }
}
