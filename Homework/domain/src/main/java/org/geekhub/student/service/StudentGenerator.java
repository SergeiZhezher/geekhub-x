package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.types.Gpa;
import org.geekhub.student.grades.types.Letter;
import org.geekhub.student.grades.types.Percentage;
import org.geekhub.student.grades.types.Ukrainian;
import org.geekhub.student.service.abstractions.StudentSource;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class StudentGenerator implements StudentSource {

    private final Random random = new Random();

    private static final String[] names = {"William", "James", "Ella", "Scarlett", "Hazel", "Addison", "Christian", "Hunter", "Robert", "Andrea"};

    @Override
    public List<Student> getStudentList(int totalStudentsCount) {
        List<Student> students = new ArrayList<>();

        for (int i = 0; i < totalStudentsCount; i++) {
            students.add(new Student(getRandomName(), getRandomGrade(), LocalDateTime.parse("2077-01-01T10:09:09")));
        }

        return students;
    }

    private String getRandomName() {
        int nameIndex = random.nextInt(names.length - 1);
        return names[nameIndex];
    }

    private Grade getRandomGrade() {
        GradeType gradeType = getRandomGradeType();
        int gradeValue = random.nextInt(100);

        switch (gradeType) {
            case GPA:
                return new Gpa(gradeValue);
            case LETTER:
                return new Letter(gradeValue);
            case PERCENTAGE:
                return new Percentage(gradeValue);
            case UKRAINIAN:
                return new Ukrainian(gradeValue);
            default:
                throw new IllegalArgumentException();
        }
    }

    private GradeType getRandomGradeType() {
        return GradeType.values()[random.nextInt(GradeType.values().length)];
    }

}
