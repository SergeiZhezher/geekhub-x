package org.geekhub.student.util.logger;

public class Logger {

    private final String classname;

    public Logger(String classname) {
        this.classname = classname;
    }

    private void log(String message) {
        System.out.println(message);
    }

    public void info(String message) {
        log("INFO: in " + classname + ". " + message);
    }

    public void warning(Throwable thr) {
        log(Color.YELLOW + " WARNING: in " + classname + ". " + thr.getMessage() + Color.RESET);
    }

    public void error(Throwable thr) {
        log(Color.RED + " ERROR: in  " + classname + ". " + thr.getMessage() + Color.RESET);
    }
}
