package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.types.Gpa;
import org.geekhub.student.grades.types.Letter;
import org.geekhub.student.grades.types.Percentage;
import org.geekhub.student.grades.types.Ukrainian;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GradeDecoder {

    public List<Integer> getDecodedGrades(List<Student> students) {
        Grade grade = students.get(0).getGrade();

        if (grade instanceof Gpa) {
            return getGpaGradesInOneHundredScoreSystem(students);
        } else if (grade instanceof Letter) {
            return getLetterGradesInOneHundredScoreSystem(students);
        } else if (grade instanceof Percentage) {
            return getPercentageGradesInOneHundredScoreSystem(students);
        } else if (grade instanceof Ukrainian) {
            return getUkrainianGradesInOneHundredScoreSystem(students);
        }
        return Collections.emptyList();
    }

    public int getDecodedGrades(Student s) {
        Grade grade = s.getGrade();

        if (grade instanceof Gpa) {
            return gpaToOneHundredScoreSystem(grade.asPrintVersion());
        } else if (grade instanceof Letter) {
            return letterToOneHundredScoreSystem(grade.asPrintVersion());
        } else if (grade instanceof Percentage) {
            return percentagesToOneHundredScoreSystem(grade.asPrintVersion());
        } else if (grade instanceof Ukrainian) {
            return ukrainianToOneHundredScoreSystem(grade.asPrintVersion());
        }
        return -1;
    }

    private List<Integer> getGpaGradesInOneHundredScoreSystem(List<Student> students) {
        return students.stream().map(s ->
                gpaToOneHundredScoreSystem(s.getGradeAsString())).collect(Collectors.toList());
    }

    private List<Integer> getLetterGradesInOneHundredScoreSystem(List<Student> students) {
        return students.stream().map(n -> letterToOneHundredScoreSystem(n.getGradeAsString())).collect(Collectors.toList());
    }

    private List<Integer> getPercentageGradesInOneHundredScoreSystem(List<Student> students) {
        return students.stream().map(s ->
                percentagesToOneHundredScoreSystem(s.getGradeAsString())).collect(Collectors.toList());
    }

    private List<Integer> getUkrainianGradesInOneHundredScoreSystem(List<Student> students) {
        return students.stream().map(s -> ukrainianToOneHundredScoreSystem(s.getGradeAsString())).collect(Collectors.toList());
    }

    private int gpaToOneHundredScoreSystem(String first) {
        return (int) Float.parseFloat(first) * 10 + 50;
    }

    private int letterToOneHundredScoreSystem(String first) {
        return (4 - (first.charAt(0) > 'E' ? first.charAt(0) - 'B' : first.charAt(0) - 'A')) * 10 + 50;
    }

    private int percentagesToOneHundredScoreSystem(String first) {
        return first.contains("<") ? 50 :
                (Integer.parseInt(first.replaceAll("[^0-9|\\s]", "").split(" ")[0]));
    }

    private int ukrainianToOneHundredScoreSystem(String first) {
        int value = Integer.parseInt(first);

        if (value >= 3 && value <= 12) {
            return (value - 2) * 10;
        }
        return (value == 2 ? 5 : 0);
    }

}
