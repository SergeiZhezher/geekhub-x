package org.geekhub.student.util.logger;

enum Color {
    RESET("\033[0m"),
    YELLOW("\033[1;33m"),
    RED("\033[1;31m");

    private final String code;

    Color(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
