package org.geekhub.student.service.abstractions;

import org.geekhub.student.dao.Student;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface StudentManager {

    void writeStudents(List<Student> generated, List<Student> read) throws IOException;
    void writeStudentsIfNotExist(List<Student> students);

    void deleteAllStudents();

    List<Student> readStudents();

    Optional<Integer> getMaxGradeValueByGradeType(String type);

    Optional<Integer> getMinGradeValueByGradeType(String type);

    Optional<Integer> getAvgGradeValueByGradeType(String type);

    Optional<Integer> getMedianGradeValueByGradeType(String type);
}
