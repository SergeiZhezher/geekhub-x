package org.geekhub.student.grades.types;

import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.util.exceptions.ScoreNotSupportedException;

public class Gpa implements Grade {

    int value;

    public Gpa(int value) {
        this.value = value;
    }

    @Override
    public String asPrintVersion() {
        if (value >= 90 && value <= 100) {
            return "4.0";
        } else if (value >= 80 && value <= 89) {
            return "3.0";
        } else if (value >= 70 && value <= 79) {
            return "2.0";
        } else if (value >= 60 && value <= 69) {
            return "1.0";
        } else if (value < 60) {
            return "0.0";
        } else {
            throw new ScoreNotSupportedException("Score value not supported: " + value);
        }
    }

    @Override
    public int getValue() {
        return value;
    }

}
