package org.geekhub.student.util.exceptions;

public class ScoreNotSupportedException extends InvalidInputException {
    public ScoreNotSupportedException(String message) {
        super(message);
    }


}
