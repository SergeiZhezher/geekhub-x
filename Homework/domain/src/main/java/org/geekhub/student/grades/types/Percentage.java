package org.geekhub.student.grades.types;

import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.util.exceptions.ScoreNotSupportedException;

public class Percentage implements Grade {

    int value;

    public Percentage(int value) {
        this.value = value;
    }

    @Override
    public String asPrintVersion() {
        if (value >= 90 && value <= 100) {
            return "90% – 100%";
        } else if (value >= 80 && value <= 89) {
            return "80% – 89%";
        } else if (value >= 70 && value <= 79) {
            return "70% – 79%";
        } else if (value >= 60 && value <= 69) {
            return "60% – 69%";
        } else if (value < 60) {
            return "<60%";
        } else {
            throw new ScoreNotSupportedException("Score value not supported: " + value);
        }
    }

    @Override
    public int getValue() {
        return value;
    }

}
