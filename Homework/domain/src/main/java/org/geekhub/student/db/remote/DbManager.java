package org.geekhub.student.db.remote;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.GradeFactoryInitialization;
import org.geekhub.student.service.GradeDecoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/sql.properties")
public class DbManager {

    private final String select;
    private final String insertStudent;
    private final String deleteStudent;
    private final String deleteGrade;
    private final String maxGradeValue;
    private final String minGradeValue;
    private final String insertGrade;
    private final String existGrade;
    private final String existStudent;

    private final GradeFactoryInitialization gradeFactory;
    private final GradeDecoder gradeDecoder;

    public DbManager(
            GradeFactoryInitialization gradeFactory,
            GradeDecoder gradeDecoder,
            @Value("${select}") String select,
            @Value("${insertStudent}") String insertStudent,
            @Value("${deleteStudent}") String deleteStudent,
            @Value("${deleteGrade}") String deleteGrade,
            @Value("${maxGradeValue}") String maxGradeValue,
            @Value("${minGradeValue}") String minGradeValue,
            @Value("${insertGrade}") String insertGrade,
            @Value("${existGrade}") String existGrade,
            @Value("${existStudent}") String existStudent
    ) {
        this.gradeFactory = gradeFactory;
        this.gradeDecoder = gradeDecoder;
        this.select = select;
        this.insertStudent = insertStudent;
        this.deleteStudent = deleteStudent;
        this.deleteGrade = deleteGrade;
        this.maxGradeValue = maxGradeValue;
        this.minGradeValue = minGradeValue;
        this.insertGrade = insertGrade;
        this.existGrade = existGrade;
        this.existStudent = existStudent;
    }

    public List<Student> readStudents(JdbcTemplate jdbcTemplate) {

        return jdbcTemplate.query(select, (rs, rowNum) -> getStudent(
                rs.getString("Name"),
                rs.getString("Grade_type").toUpperCase(),
                rs.getInt("Grade_value"),
                rs.getTimestamp("Grade_time")
        ));
    }

    private Student getStudent(String studentName, String gradeType, int gradeValue, Timestamp gradeTime) {

        return new Student(
                studentName,
                gradeFactory.createGrade(GradeType.valueOf(gradeType), gradeValue),
                gradeTime.toLocalDateTime()
        );
    }

    @Transactional
    public void write(List<Student> students, JdbcTemplate jdbcTemplate) {

        for (Student student : students) {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            jdbcTemplate.update(connection -> insertGradeAndGetIdKey(connection, student), keyHolder);

            jdbcTemplate.update(insertStudent,
                    student.getName(),
                    Timestamp.valueOf(student.getGradeTime()),
                    Objects.requireNonNull(keyHolder.getKeys()).get("id")
            );
        }

    }

    public void writeIfNotExist(List<Student> students, JdbcTemplate jdbcTemplate) {
        List<Student> databaseWriteCandidates = new ArrayList<>();

        for (Student student : students) {
            Boolean isExistGrade = jdbcTemplate.queryForObject(existGrade,
                    Boolean.class, student.getGrade().getClass().getSimpleName().toUpperCase(),
                    gradeDecoder.getDecodedGrades(student));

            Boolean isExistStudent = jdbcTemplate.queryForObject(existStudent,
                    Boolean.class, student.getName(), Timestamp.valueOf(student.getGradeTime()));

            if (!isExistGrade || !isExistStudent) {
                databaseWriteCandidates.add(student);
            }
        }

        write(databaseWriteCandidates, jdbcTemplate);
    }

    @Transactional
    public void delete(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update(deleteStudent);
        jdbcTemplate.update(deleteGrade);
    }

    public int getMaxGradeValueByGradeType(String type, JdbcTemplate jdbcTemplate) {

        return Optional.ofNullable(jdbcTemplate.queryForObject(
                maxGradeValue, Integer.class, type)).orElse(0);
    }

    public int getMinGradeValueByGradeType(String type, JdbcTemplate jdbcTemplate) {

        return Optional.ofNullable(jdbcTemplate.queryForObject(
                minGradeValue, Integer.class, type)).orElse(0);
    }

    private PreparedStatement insertGradeAndGetIdKey(Connection connection, Student student) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(
                insertGrade, Statement.RETURN_GENERATED_KEYS);

        ps.setString(1, student.getGrade().getClass().getSimpleName().toUpperCase());
        ps.setInt(2, gradeDecoder.getDecodedGrades(student));

        return ps;
    }

}
