package org.geekhub.student.grades.service;

import org.geekhub.student.grades.types.Ukrainian;
import org.geekhub.student.util.exceptions.GradeNotSupportedException;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.types.Gpa;
import org.geekhub.student.grades.types.Letter;
import org.geekhub.student.grades.types.Percentage;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class GradeFactoryInitialization implements GradeFactory {

    @Override
    public Grade createGrade(GradeType gradeType, int value) {
        Optional.ofNullable(gradeType).orElseThrow(GradeNotSupportedException::new);

        if (gradeType.equals(GradeType.GPA)) {
            return new Gpa(value);
        } else if (gradeType.equals(GradeType.LETTER)) {
            return new Letter(value);
        } else if (gradeType.equals(GradeType.PERCENTAGE)) {
            return new Percentage(value);
        } else if (gradeType.equals(GradeType.UKRAINIAN)) {
            return new Ukrainian(value);
        } else {
            throw new GradeNotSupportedException(gradeType + " is unknown grade");
        }
    }

}
