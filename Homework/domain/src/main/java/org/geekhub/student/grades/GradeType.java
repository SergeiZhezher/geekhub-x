package org.geekhub.student.grades;

public enum GradeType {
    LETTER, PERCENTAGE, GPA, UKRAINIAN
}

