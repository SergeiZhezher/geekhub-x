package org.geekhub.student.grades.service;

import org.geekhub.student.grades.GradeType;

public interface GradeFactory {

    Grade createGrade(GradeType gradeType, int value);

}

