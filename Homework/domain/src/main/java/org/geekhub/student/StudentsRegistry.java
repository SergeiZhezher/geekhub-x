package org.geekhub.student;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.service.StudentGrouper;
import org.geekhub.student.service.StudentSorter;
import org.geekhub.student.service.abstractions.StudentManager;
import org.geekhub.student.service.abstractions.StudentOutput;
import org.geekhub.student.service.abstractions.StudentSource;
import org.geekhub.student.util.logger.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class StudentsRegistry {

    private final Logger logger = new Logger(StudentsRegistry.class.getName());

    private final StudentGrouper studentGrouper;
    private final StudentSorter studentSorter;
    private final StudentManager studentManager;
    private final StudentOutput studentOutput;

    public StudentsRegistry(
            StudentGrouper studentGrouper,
            StudentSorter studentSorter,
            StudentManager studentManager,
            StudentOutput studentOutput
    ) {
        this.studentGrouper = studentGrouper;
        this.studentSorter = studentSorter;
        this.studentManager = studentManager;
        this.studentOutput = studentOutput;
    }

    public void startStudentRegistration(int totalStudentsCount, StudentSource studentSource) {

        List<Student> generatedStudents = studentSource.getStudentList(totalStudentsCount);
        List<Student> readStudents = studentManager.readStudents();

        try {
            studentManager.writeStudents(generatedStudents, readStudents);
        } catch (IllegalArgumentException | IOException e) {
            logger.warning(e);
        }

        generatedStudents.addAll(readStudents);
        registerStudents(generatedStudents);
    }

    private void registerStudents(List<Student> studentList) {
        Map<GradeType, List<Student>> groupedStudents = studentGrouper.getGroupedStudents(studentList);

        printSortedStudentsByGrade(groupedStudents);
        printSortedStudentsByName(studentList);
    }

    private void printSortedStudentsByGrade(Map<GradeType, List<Student>> groupedStudents) {
        Map<GradeType, List<Student>> students = studentSorter.getSortedListByGrade(groupedStudents);

        studentOutput.printTableList(students);
    }

    private void printSortedStudentsByName(List<Student> studentList) {
        List<Student> sortedStudentList = studentSorter.getSortedListByName(studentList);
        studentOutput.printTableList(sortedStudentList);
    }

}
