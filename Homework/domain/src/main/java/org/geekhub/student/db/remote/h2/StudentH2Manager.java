package org.geekhub.student.db.remote.h2;

import org.geekhub.student.dao.Student;
import org.geekhub.student.db.remote.DbManager;
import org.geekhub.student.service.abstractions.StudentManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@ConditionalOnProperty(value = "datasource.storage-type", havingValue = "H2")
public class StudentH2Manager implements StudentManager {

    private final String avgGradeValueForH2;
    private final String medianGradeValueForH2;

    private final DbManager dbManager;
    private final JdbcTemplate jdbcTemplate;

    public StudentH2Manager(
            DbManager dbManager,
            @Qualifier("jdbcH2") JdbcTemplate jdbcTemplate,
            @Value("${avgGradeValueForH2}") String avgGradeValueForH2,
            @Value("${medianGradeValueForH2}") String medianGradeValueForH2
    ) {
        this.jdbcTemplate = jdbcTemplate;
        this.dbManager = dbManager;
        this.avgGradeValueForH2 = avgGradeValueForH2;
        this.medianGradeValueForH2 = medianGradeValueForH2;
    }

    @Override
    public List<Student> readStudents() {
        return dbManager.readStudents(jdbcTemplate);
    }

    @Override
    public void writeStudents(List<Student> generated, List<Student> read) {
        List<Student> students = new ArrayList<>(generated);

        dbManager.write(students, jdbcTemplate);
    }

    @Override
    public void deleteAllStudents() {
        dbManager.delete(jdbcTemplate);
    }

    @Override
    public Optional<Integer> getMaxGradeValueByGradeType(String type) {
        int max = dbManager.getMaxGradeValueByGradeType(type, jdbcTemplate);

        return max == 0 ? Optional.empty() : Optional.of(max);
    }

    @Override
    public Optional<Integer> getMinGradeValueByGradeType(String type) {
        int min = dbManager.getMinGradeValueByGradeType(type, jdbcTemplate);

        return min == 0 ? Optional.empty() : Optional.of(min);
    }

    @Override
    public Optional<Integer> getAvgGradeValueByGradeType(String type) {
        return Optional.ofNullable(jdbcTemplate.queryForObject(
                avgGradeValueForH2, Integer.class, type));
    }

    @Override
    public Optional<Integer> getMedianGradeValueByGradeType(String type) {
        return Optional.ofNullable(jdbcTemplate.queryForObject(
                medianGradeValueForH2, Integer.class, type));
    }

    @Override
    public void writeStudentsIfNotExist(List<Student> students) {
        dbManager.writeIfNotExist(students, jdbcTemplate);
    }
}
