package org.geekhub.student.util.modes;

public enum ProgramMode {
    MANUAL,
    RANDOM,
    NONE
}
