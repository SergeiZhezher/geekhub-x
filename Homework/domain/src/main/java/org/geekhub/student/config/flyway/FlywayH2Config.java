package org.geekhub.student.config.flyway;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ConditionalOnProperty(value = "datasource.storage-type", havingValue = "H2")
public class FlywayH2Config {

    @Bean(name = "flywayH2")
    public Flyway flywayH2(@Qualifier("h2") DataSource dataSource) {

        return Flyway.configure()
                .ignoreFutureMigrations(true)
                .ignoreMissingMigrations(true)
                .outOfOrder(true)
                .locations("classpath:db/migration/h2")
                .dataSource(dataSource)
                .load();
    }

    @Bean
    public InitializingBean flywayMigrateH2(@Qualifier("flywayH2") Flyway flyway) {
        return flyway::migrate;
    }

}
