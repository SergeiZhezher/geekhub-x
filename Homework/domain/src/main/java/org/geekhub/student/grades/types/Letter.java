package org.geekhub.student.grades.types;

import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.util.exceptions.ScoreNotSupportedException;

public class Letter implements Grade {

    int value;

    public Letter(int value) {
        this.value = value;
    }

    @Override
    public String asPrintVersion() {
        if (value >= 90 && value <= 100) {
            return "A";
        } else if (value >= 80 && value <= 89) {
            return "B";
        } else if (value >= 70 && value <= 79) {
            return "C";
        } else if (value >= 60 && value <= 69) {
            return "D";
        } else if (value < 60) {
            return "F";
        } else {
            throw new ScoreNotSupportedException("Score value not supported: " + value);
        }
    }

    @Override
    public int getValue() {
        return value;
    }

}
