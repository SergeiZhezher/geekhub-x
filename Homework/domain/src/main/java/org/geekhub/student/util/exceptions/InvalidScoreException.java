package org.geekhub.student.util.exceptions;

public class InvalidScoreException extends IllegalArgumentException {
    public InvalidScoreException(String message) {
        super(message);
    }
}
