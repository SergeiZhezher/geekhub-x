package org.geekhub.student.service.analytics;

import org.geekhub.student.service.abstractions.StudentManager;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class StudentAnalytic implements StudentAnalytics {

    private final StudentManager studentManager;

    public StudentAnalytic(StudentManager studentManager) {
        this.studentManager = studentManager;
    }

    @Override
    public Optional<Integer> averageScore(String type) {
        return studentManager.getAvgGradeValueByGradeType(type);
    }

    @Override
    public Optional<Integer> maxScore(String type) {
        return studentManager.getMaxGradeValueByGradeType(type);
    }

    @Override
    public Optional<Integer> minScore(String type) {
        return studentManager.getMinGradeValueByGradeType(type);
    }

    @Override
    public Optional<Integer> medianScore(String type) {
        return studentManager.getMedianGradeValueByGradeType(type);
    }

}
