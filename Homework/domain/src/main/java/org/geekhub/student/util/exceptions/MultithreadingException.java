package org.geekhub.student.util.exceptions;

public class MultithreadingException extends RuntimeException {

    public MultithreadingException(String message) {
        super(message);
    }

}
