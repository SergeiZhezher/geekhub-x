package org.geekhub.student.grades.service;


import java.io.Serializable;

public interface Grade extends Serializable {
    String asPrintVersion();
    int getValue();
}

