create table usr
(
    id        uuid       not null,
    name      varchar(255) not null unique,
    password  varchar(255) not null,
    is_active boolean      not null,
    primary key (id)
);