create table IF NOT EXISTS grade
(
    id          integer auto_increment primary key,
    grade_type  varchar(32) not null,
    grade_value integer     not null
);