alter table student alter column grade_id DROP DEFAULT;
alter table student alter column grade_id set not null;