create table IF NOT EXISTS student
(
    id         serial primary key,
    name       varchar(128) not null,
    grade_time timestamp    not null,
    grade_id   serial       not null,
    constraint student_grade_fk foreign key (grade_id) references grade (id)
);