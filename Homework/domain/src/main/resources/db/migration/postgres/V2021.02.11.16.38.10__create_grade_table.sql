create table IF NOT EXISTS grade
(
    id          serial primary key,
    grade_type  varchar(32) not null,
    grade_value integer     not null
);