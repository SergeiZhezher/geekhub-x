package org.geekhub.student.grades.service;

import org.geekhub.student.grades.types.Ukrainian;
import org.geekhub.student.util.exceptions.GradeNotSupportedException;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.types.Gpa;
import org.geekhub.student.grades.types.Letter;
import org.geekhub.student.grades.types.Percentage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GradeFactoryInitializationTest {

    private final GradeFactoryInitialization gradeFactoryInitialization = new GradeFactoryInitialization();

    @Test
    void createGradeGpaTest() {
        assertTrue(gradeFactoryInitialization.createGrade(GradeType.GPA, 100) instanceof Gpa);
    }

    @Test
    void createGradeLetterTest() {
        assertTrue(gradeFactoryInitialization.createGrade(GradeType.LETTER, 100) instanceof Letter);
    }

    @Test
    void createGradePercentageTest() {
        assertTrue(gradeFactoryInitialization.createGrade(GradeType.PERCENTAGE, 100) instanceof Percentage);
    }

    @Test
    void createGradeUkrainianTest() {
        assertTrue(gradeFactoryInitialization.createGrade(GradeType.UKRAINIAN, 100) instanceof Ukrainian);
    }

    @Test()
    void failCreateGradeTest() {
        assertThrows(GradeNotSupportedException.class, () -> gradeFactoryInitialization.createGrade(null, 100));
    }

}
