package org.geekhub.student.grades.types;

import org.geekhub.student.util.exceptions.ScoreNotSupportedException;
import org.geekhub.student.grades.service.Grade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GradeTypeTest {

    private final int[] gradeValues = {50, 60, 70, 80, 90};

    @Test
    void asPrintVersionGradeTest() {
        String[] gpaReply = {"0.0", "1.0", "2.0", "3.0", "4.0"};
        for (int i = 0; i < gpaReply.length; i++) {
            assertEquals(new Gpa(gradeValues[i]).asPrintVersion(), gpaReply[i]);
        }
    }

    @Test
    void asPrintVersionLetterTest() {
        String[] letterReply = {"F", "D", "C", "B", "A"};
        for (int i = 0; i < letterReply.length; i++) {
            assertEquals(new Letter(gradeValues[i]).asPrintVersion(), letterReply[i]);
        }
    }

    @Test
    void asPrintVersionPercentageTest() {
        String[] percentageReply = {"<60%", "60% – 69%", "70% – 79%", "80% – 89%", "90% – 100%"};
        for (int i = 0; i < percentageReply.length; i++) {
            assertEquals(new Percentage(gradeValues[i]).asPrintVersion(), percentageReply[i]);
        }
    }

    @Test
    void asPrintVersionUkrainianTest() {
        String[] ukrainianReply = {"7", "8", "9", "10", "11"};
        for (int i = 0; i < ukrainianReply.length; i++) {
            assertEquals(new Ukrainian(gradeValues[i] + 9).asPrintVersion(), ukrainianReply[i]);
        }
    }

    static Object[][] failAsPrintVersionProvider() {
        return new Object[][]{
                {new Gpa(160)}, {new Letter(160)}, {new Percentage(160)}, {new Ukrainian(160)}
        };
    }

    @ParameterizedTest
    @MethodSource("failAsPrintVersionProvider")
    void failAsPrintVersion(Grade grade) {
        assertThrows(ScoreNotSupportedException.class, grade::asPrintVersion);
    }

}

