package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class StudentGeneratorTest {

    private final int studentCount = 3;
    private List<Student> students;

    @BeforeEach
    private void setUp() {
        StudentGenerator studentGenerator = new StudentGenerator();
        students = studentGenerator.getStudentList(studentCount);
    }

    @Test
    void getGeneratedStudentsTest() {
        assertEquals(studentCount, students.size());
    }

    @Test
    void generated_name_is_not_null() {
        for (Student s : students) {
            assertNotNull(s.getName());
        }
    }

    @Test
    void generated_gradeType_is_not_null() {
        for (Student s : students) {
            assertNotNull(s.getGrade());
        }
    }

    @Test
    void generated_grade_is_not_null() {
        for (Student s : students) {
            assertNotNull(s.getGradeAsString());
        }
    }

    @Test
    void generated_gradeTime_is_not_null() {
        for (Student s : students) {
            assertNotNull(s.getGradeTime());
        }
    }
}
