package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.types.Gpa;
import org.geekhub.student.grades.types.Letter;
import org.geekhub.student.grades.types.Percentage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

class StudentSorterTest {

    private final List<Student> nameStudentGroup = new ArrayList<>();
    private final StudentSorter studentSorter = new StudentSorter();
    private Map<GradeType, List<Student>> students;

    @BeforeEach
    private void setUp() {
        String[] namesGet = {"aFTer", "end", "before"};
        int[] gradesGet = {60, 100, 80};

        List<Student> studentGroup = new ArrayList<>();

        for (int i = 0; i < namesGet.length; i++) {
            studentGroup.add(new Student(null, new Gpa(gradesGet[i]), LocalDateTime.now()));
            studentGroup.add(new Student(null, new Letter(gradesGet[i]), LocalDateTime.now()));
            studentGroup.add(new Student(null, new Percentage(gradesGet[i]), LocalDateTime.now()));

            nameStudentGroup.add(new Student(namesGet[i], null, LocalDateTime.now()));
        }

        students = new StudentGrouper().getGroupedStudents(studentGroup);
        students = new StudentSorter().getSortedListByGrade(students);
    }

    @Test
    void getSortedListByGpaTest() {
        String[] gpaReturn = {"4.0", "3.0", "1.0"};
        assertTrue(isEqualsArrayByGrade(gpaReturn, students.get(GradeType.GPA)));
    }

    @Test
    void getSortedListByLetterTest() {
        String[] letterReturn = {"A", "B", "D"};
        assertTrue(isEqualsArrayByGrade(letterReturn, students.get(GradeType.LETTER)));
    }

    @Test
    void getSortedListByPercentageTest() {
        String[] percentageReturn = {"90% – 100%", "80% – 89%", "60% – 69%"};
        assertTrue(isEqualsArrayByGrade(percentageReturn, students.get(GradeType.PERCENTAGE)));
    }

    @Test
    void getSortedListByNameTest() {
        String[] namesReturn = {"aFTer", "before", "end"};
        boolean status = isEqualsArrayByName(namesReturn, studentSorter.getSortedListByName(nameStudentGroup));
        assertTrue(status);
    }

    private boolean isEqualsArrayByGrade(String[] gradeReturn, List<Student> students) {
        for (int i = 0; i < gradeReturn.length; i++) {
            if (!gradeReturn[i].equals(students.get(i).getGradeAsString())) {
                return false;
            }
        }
        return true;
    }

    private boolean isEqualsArrayByName(String[] nameReturn, List<Student> students) {
        for (int i = 0; i < nameReturn.length; i++) {
            if (!nameReturn[i].equals(students.get(i).getName())) {
                return false;
            }
        }
        return true;
    }
}
