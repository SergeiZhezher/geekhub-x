package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.types.Gpa;
import org.geekhub.student.grades.types.Letter;
import org.geekhub.student.grades.types.Percentage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.fail;

class StudentGrouperTest {

    private final Grade[] grades = {new Gpa(0), new Letter(0), new Percentage(0)};
    private final List<Student> students = new ArrayList<>();

    private StudentGrouper studentGrouper;

    @BeforeEach
    private void setUp() {
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            students.add(new Student(null, grades[random.nextInt(grades.length)], LocalDateTime.now()));
        }
        studentGrouper = new StudentGrouper();
    }

    @Test()
    void getGroupedStudentsTest() {
        fail_test_if_grouping_is_not_correct(studentGrouper.getGroupedStudents(students));
    }

    private void fail_test_if_grouping_is_not_correct(Map<GradeType, List<Student>> groupedStudents) {
        groupedStudents.forEach((gradeType, list) -> {
            for (Student s : list) {
                String className = s.getGrade().getClass().getName().toLowerCase();
                if (!className.endsWith(gradeType.name().toLowerCase())) {
                    fail();
                }
            }
        });
    }

}
