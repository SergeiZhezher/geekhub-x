package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.service.GradeFactoryInitialization;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GradeDecoderTest {

    private GradeDecoder gradeDecoder;
    private static GradeFactoryInitialization gradeFactory;

    private static List<Student> studentsGpa;
    private static List<Student> studentsLetter;
    private static List<Student> studentsUkrainian;
    private static List<Student> studentsPercentage;

    @BeforeEach
    void setUp() {
        gradeDecoder = new GradeDecoder();
        gradeFactory = new GradeFactoryInitialization();

        studentsGpa = getFilledList(GradeType.GPA);
        studentsLetter = getFilledList(GradeType.LETTER);
        studentsUkrainian = getFilledList(GradeType.UKRAINIAN);
        studentsPercentage = getFilledList(GradeType.PERCENTAGE);
    }

    @Test
    void gpa_decoding_test() {
        assertTrue(isValidGrades(studentsGpa, gradeDecoder.getDecodedGrades(studentsGpa), GradeType.GPA));
    }

    @Test
    void letter_decoding_test() {
        assertTrue(isValidGrades(studentsLetter, gradeDecoder.getDecodedGrades(studentsLetter), GradeType.LETTER));
    }

    @Test
    void percentage_decoding_test() {
        assertTrue(isValidGrades(studentsPercentage, gradeDecoder.getDecodedGrades(studentsPercentage), GradeType.PERCENTAGE));
    }

    @Test
    void ukrainian_decoding_test() {
        assertTrue(isValidGrades(studentsUkrainian, gradeDecoder.getDecodedGrades(studentsUkrainian), GradeType.UKRAINIAN));
    }

    @Test
    void fail_decoding_one_student_test() {
        Student student = new Student(null, null, null);
        assertEquals(gradeDecoder.getDecodedGrades(student), -1);
    }

    @Test
    void fail_decoding_list_students_test() {
        List<Student> students = List.of(new Student(null, null, null));
        assertTrue(gradeDecoder.getDecodedGrades(students).isEmpty());
    }

    static List<Student> getFilledList(GradeType gradeType) {
        List<Student> students = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            students.add(new Student(null, gradeFactory.createGrade(gradeType, i * 10), null));
        }

        return students;
    }

    static boolean isValidGrades(List<Student> students, List<Integer> grades, GradeType gradeType) {

        for (int i = 0; i < students.size(); i++) {
            String gradeAsString = students.get(i).getGradeAsString();
            Grade grade = gradeFactory.createGrade(gradeType, grades.get(i));
            if (!gradeAsString.equals(grade.asPrintVersion())) {
                return false;
            }
        }

        return true;
    }

}
