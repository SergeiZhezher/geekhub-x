package org.geekhub.student.dao;

import org.geekhub.student.grades.types.Gpa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StudentTest {

    private Student student;
    private Gpa gpa;

    private final String name = "name";
    private final String testTime = "2077-01-01T10:09:09";

    @BeforeEach
    private void setUp() {
        gpa = new Gpa(100);

        student = new Student(name, gpa, LocalDateTime.parse(testTime));
    }

    @Test
    void getNameTest() {
        assertEquals(student.getName(), name);
    }

    @Test
    void getGradeTest() {
        assertEquals(student.getGrade(), gpa);
    }

    @Test
    void getGradeAsStringTest() {
        assertEquals(student.getGradeAsString(), "4.0");
    }

    @Test
    void getGradeTimeTest() {
        assertEquals(student.getGradeTime().toString(), testTime);
    }
}
