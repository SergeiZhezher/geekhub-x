delete from USER_ROLE;
delete from USR;

-- admin:admin
insert into usr (id, name, password, is_active)
values ('4e38099a-83f1-11eb-8dcd-0242ac130003', 'admin', '$2y$12$6g.L.xDK6W4We1z.bYi7eulOvcCfpSa/0IOt1m0vFsiAcHbXr/bjm', true);

insert into user_role (user_id, roles)
values ('4e38099a-83f1-11eb-8dcd-0242ac130003', 'ADMIN');


-- user:user
insert into usr (id, name, password, is_active)
values ('4e380b7a-83f1-11eb-8dcd-0242ac130003', 'user', '$2y$12$mPJ4BzMxWOszVRFpmCKky.ZdMkmjiE2tZfawppWfJ7GeoNTWVWway', true);

insert into user_role (user_id, roles)
values ('4e380b7a-83f1-11eb-8dcd-0242ac130003', 'USER');