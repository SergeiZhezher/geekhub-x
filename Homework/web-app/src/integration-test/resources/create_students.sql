delete from STUDENT;
delete from GRADE;

insert into grade(grade_type, grade_value) values('GPA', 100);
insert into student(name, grade_time, GRADE_ID) values('Vincent', current_timestamp, (select ID from GRADE where GRADE_TYPE = 'GPA' and GRADE_VALUE = 100));