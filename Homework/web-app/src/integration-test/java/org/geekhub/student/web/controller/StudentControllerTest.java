package org.geekhub.student.web.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("admin")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create_students.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void get_students_test() throws Exception {
        mockMvc.perform(get("/students"))
                .andExpect(status().isNoContent());
    }

    @Test
    void get_analytics_test() throws Exception {
        mockMvc.perform(get("/students/analytics"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(16)));
    }

    @Test
    void add_student_test() throws Exception {
        mockMvc.perform(post("/students")
                .param("name", "name")
                .param("type", "LETTER")
                .param("value", "100")
                .param("date", "2077-01-01T10:09:09")
                .with(csrf())
        ).andExpect(status().isAccepted());
    }

    @Test
    void manual_registration_students_test() throws Exception {
        mockMvc.perform(post("/students/registration")
                .param("request", "manual")
                .param("totalStudentsCount", "0")
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Test
    void auto_registration_students_test() throws Exception {
        mockMvc.perform(post("/students/registration")
                .param("request", "auto")
                .param("totalStudentsCount", "5")
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Test
    void delete_students_test() throws Exception {
        mockMvc.perform(delete("/students").with(csrf()))
                .andExpect(status().isNoContent());
    }

    @ParameterizedTest
    @MethodSource("fail_add_student_provider")
    void fail_add_student_test(String name, String type, String value, String date) throws Exception {
        mockMvc.perform(post("/students")
                .param("name", name)
                .param("type", type)
                .param("value", value)
                .param("date", date)
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    static Object[][] fail_add_student_provider() {
        return new Object[][]{
                {"name", "GPA", "100", "Invalid date"},
                {"name", "GPA", "Invalid grade value", "2077-01-01T10:09:09"},
                {"name", "Invalid grade type", "100", "2077-01-01T10:09:09"},
                {"", "Invalid grade type", "100", "2077-01-01T10:09:09"}
        };
    }

    @ParameterizedTest
    @MethodSource("fail_registration_students_provider")
    void fail_registration_students_test(String mode, String studentsCount) throws Exception {
        mockMvc.perform(post("/students/registration")
                .param("request", mode)
                .param("totalStudentsCount", studentsCount)
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    static Object[][] fail_registration_students_provider() {
        return new Object[][]{
                {"Invalid registration mode ", "10"},
                {"auto", "Invalid students count"}
        };
    }

}
