package org.geekhub.student.repository;

import org.geekhub.student.config.FlywayTestConfig;
import org.geekhub.student.dao.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ContextConfiguration(classes = {FlywayTestConfig.class})
class UserRepositoryTest {

    private final static String USER_NAME = "user";

    @Autowired
    private UserRepository userRepository;

    @Test
    void find_by_name_test() {
        User user = userRepository.findByName(USER_NAME).orElseGet(User::new);
        assertEquals(user.getName(), USER_NAME);
    }

    @Test
    void existsByName() {
        assertTrue(userRepository.existsByName(USER_NAME));
    }

}
