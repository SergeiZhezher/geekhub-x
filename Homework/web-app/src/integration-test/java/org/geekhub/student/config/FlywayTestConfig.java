package org.geekhub.student.config;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@TestConfiguration
public class FlywayTestConfig {

    @Bean
    public Flyway flywayH2(DataSource dataSource) {

        return Flyway.configure()
                .ignoreFutureMigrations(true)
                .ignoreMissingMigrations(true)
                .outOfOrder(true)
                .locations("classpath:db/migration/h2")
                .dataSource(dataSource)
                .load();
    }

    @Bean
    public InitializingBean flywayMigrateH2(Flyway flyway) {
        return flyway::migrate;
    }

}
