package org.geekhub.student.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("admin")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create_users.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class UserManagerControllerTest {

    private final static String USER_NAME = "user";
    private final static String ADMIN_NAME = "admin";
    private final static String USER_ID = "4e380b7a-83f1-11eb-8dcd-0242ac130003";
    private final static String ADMIN_ID = "4e38099a-83f1-11eb-8dcd-0242ac130003";

    @Autowired
    private MockMvc mockMvc;

    @Test
    void get_user_test() throws Exception {
        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].name", containsInAnyOrder(ADMIN_NAME, USER_NAME)));
    }

    @Test
    void add_user_test() throws Exception {
        mockMvc.perform(post("/users")
                .param("name", "name")
                .param("password", "password")
                .param("confirmedPassword", "password")
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Test
    void update_role_test() throws Exception {
        mockMvc.perform(patch("/users")
                .param("id", USER_ID)
                .param("role", "ADMIN")
                .with(csrf())
        ).andExpect(status().isNoContent());
    }

    @Test
    void delete_test() throws Exception {
        mockMvc.perform(delete("/users")
                .param("id", USER_ID)
                .with(csrf())
        ).andExpect(status().isNoContent());
    }

    @Test
    void fail_add_user_without_name_test() throws Exception {
        mockMvc.perform(post("/users")
                .param("name", "")
                .param("password", "123")
                .param("confirmedPassword", "123")
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_add_user_with_not_equals_passwords_test() throws Exception {
        mockMvc.perform(post("/users")
                .param("name", "name")
                .param("password", "123")
                .param("confirmedPassword", "")
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    @Test
    void fail_add_user_with_already_exist_name_test() throws Exception {
        mockMvc.perform(post("/users")
                .param("name", ADMIN_NAME)
                .param("password", "123")
                .param("confirmedPassword", "123")
                .with(csrf())
        ).andExpect(status().isConflict());
    }

    @Test
    void fail_update_role_test() throws Exception {
        mockMvc.perform(patch("/users")
                .param("id", ADMIN_ID)
                .param("role", "ADMIN")
                .with(csrf())
        ).andExpect(status().isForbidden());
    }

    @Test
    void fail_delete_test() throws Exception {
        mockMvc.perform(delete("/users")
                .param("id", ADMIN_ID)
                .with(csrf())
        ).andExpect(status().isForbidden());
    }

}
