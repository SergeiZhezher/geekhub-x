let token = $('#_csrf').attr('content');
let header = $('#_csrf_header').attr('content');

function load_students() {
    $.get(
        "/students",
        function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                for (const [type, students] of Object.entries(data)) {
                    for (let student of students) {
                        $("#student-container").append(
                            '<tr>' +
                            '<td>' + student.name + '</td>' +
                            '<td>' + student.gradeTime + '</td>' +
                            '<td>' + type + '</td>' +
                            '<td>' + student.grade.value + '</td>' +
                            '</tr>'
                        );
                    }
                }
            }
        }
    );
}

function load_analytics() {
    $.get(
        "/students/analytics",
        function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                for (let i = 0; i < data.length; i++) {
                    $("#analytic-container").append(data[i] + '<br/>');
                }
            }
        }
    );
}

function start_students_registration() {

    let number_of_students = prompt("please enter the number of students", "4");
    let program_mode = prompt("please enter program mode (RANDOM/MANUAL)", "RANDOM");

    if (program_mode === "RANDOM") {
        auto_registration(number_of_students)
    } else if (program_mode === "MANUAL") {
        manual_registration(number_of_students);
    }
}

function auto_registration(number_of_students) {
    $.ajax({
        type: "POST",
        url: "/students/registration",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            totalStudentsCount: number_of_students,
            request: "auto"
        },
        success: function (data, textStatus, xhr) {
            reload_page();
        }
    });
}

function manual_registration(number_of_students) {

    alert("Warnings! Students entered incorrectly will not be registered");

    for (let i = 0; i < number_of_students; i++) {
        let name = prompt(i + 1 + " please enter name", "I am a man");
        let grade_type = prompt(i + 1 + " please enter grade type (GPA, LETTER, PERCENTAGE, UKRAINIAN)", "GPA");
        let grade_value = prompt(i + 1 + " please enter grade value (0-100)", "80");
        let date_time = prompt(i + 1 + " please enter Date-time (2077-01-01T10:09:09)", "2077-01-01T10:09:09");

        send_student_to_server(name, grade_type, grade_value, date_time);
    }

    start_manual_student_registration();
}

function send_student_to_server(name, type, value, date) {
    $.ajax({
        type: "POST",
        url: "/students",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            name: name,
            type: type,
            value: value,
            date: date
        }
    });
}

function start_manual_student_registration() {
    $.ajax({
        type: "POST",
        url: "/students/registration",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            totalStudentsCount: 0,
            request: "manual"
        },
        success: function (data, textStatus, xhr) {
            reload_page();
        }
    });
}

function remove_all_student() {
    $.ajax({
        type: "DELETE",
        url: "/students",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        success: function (data, textStatus, xhr) {
            reload_page();
        }
    });
}

function reload_page() {
    setTimeout(function () {
        document.location.reload();
    }, 200);
}