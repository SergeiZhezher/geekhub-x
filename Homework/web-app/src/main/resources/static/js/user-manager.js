let token = $('#_csrf').attr('content');
let header = $('#_csrf_header').attr('content');

function create_user() {
    let name = prompt("Name: ");
    let password = prompt("Password: ");
    let confirmed_password = prompt("Confirm password: ");

    $.ajax({
        type: "POST",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            name: name,
            password: password,
            confirmedPassword: confirmed_password
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 201) {
                reload_page();
            }
        }
    });
}

function load_users() {
    $.ajax({
        type: "GET",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                for (let user of data) {

                    $("#users-container").append(
                        '<tr data-id="' + user.id + '"><td>' + user.name + '</td>' +
                        '<td> ' +
                        '<select onchange="update_role(this);" id="test">' +
                        '<option value="' + user.id + '">USER</option>' +
                        '<option value="' + user.id + '">ADMIN</option>' +
                        '<option disabled selected="selected">' + user.roles + '</option>' +
                        '</select>  ' +
                        '</td>' +
                        '<td><button data-id="' + user.id + '" onclick="delete_user(this)">remove</button></td></tr>'
                    );

                }
            }
        }
    });
}

function update_role(val) {
    let option = val.options[val.selectedIndex];

    $.ajax({
        type: "PATCH",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            id: option.value,
            role: option.text,
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                console.log("Success update")
                reload_page();
            }
        }
    });
}

function delete_user(elm) {
    let id = $(elm).attr("data-id");

    $.ajax({
        type: "DELETE",
        url: "/users",
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
        data: {
            id: id
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                $('tr[data-id="' + id + '"]').remove();
                console.log("Success delete")
            }
        }
    });
}

function reload_page() {
    setTimeout(function () {
        document.location.reload();
    }, 200);
}