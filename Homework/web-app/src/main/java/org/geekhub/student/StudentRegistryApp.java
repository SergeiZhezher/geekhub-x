package org.geekhub.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class StudentRegistryApp {

    public static void main(String[] args) {
        SpringApplication.run(StudentRegistryApp.class, args);
    }

}
