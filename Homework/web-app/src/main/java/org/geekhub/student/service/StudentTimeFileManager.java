package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.util.logger.Logger;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

@Component
public class StudentTimeFileManager {

    private final Logger logger = new Logger(StudentTimeFileManager.class.getName());

    public void writeToTimeFile(Map<GradeType, List<Student>> studentsList) {
        Map<GradeType, List<Student>> students = new HashMap<>(studentsList);

        File file = getStudentsFilePath("time_storage_as_map").toFile();

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file.getAbsolutePath()))) {

            oos.writeObject(students);
            oos.flush();

        } catch (IOException e) {
            logger.warning(e);
        }
    }

    public Map<GradeType, List<Student>> readFromTimeFile(Map<GradeType, List<Student>> studentsList) {
        File file = getStudentsFilePath("time_storage_as_map").toFile();

        if (!file.exists()) {
            return Collections.emptyMap();
        }
        Map<GradeType, List<Student>> students;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file.getAbsolutePath()))) {
            Object obj = ois.readObject();

            students = obj instanceof Map ? (Map<GradeType, List<Student>>) obj : Collections.emptyMap();

        } catch (IOException | ClassNotFoundException e) {
            logger.warning(e);
            return Collections.emptyMap();
        }

        return students;
    }

    public void writeToTimeFile(List<Student> studentsList) {
        List<Student> students = new ArrayList<>(studentsList);

        File file = getStudentsFilePath("time_storage_as_list").toFile();

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file.getAbsolutePath()))) {

            oos.writeObject(students);
            oos.flush();

        } catch (IOException e) {
            logger.warning(e);
        }
    }

    public List<Student> readFromTimeFile(List<Student> studentsList) {
        File file = getStudentsFilePath("time_storage_as_list").toFile();

        if (!file.exists()) {
            return Collections.emptyList();
        }
        List<Student> students;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file.getAbsolutePath()))) {
            Object obj = ois.readObject();

            students = obj instanceof List ? (List<Student>) obj : Collections.emptyList();

        } catch (IOException | ClassNotFoundException e) {
            logger.warning(e);
            return Collections.emptyList();
        }
        file.delete();

        return students;
    }

    public void delete() {
        File file = getStudentsFilePath("time_storage_as_map").toFile();
        file.delete();
    }

    private Path getStudentsFilePath(String name) {
        String path = System.getProperty("user.home");
        Path homePath = Path.of(path);
        return homePath.resolve(name + ".ser");
    }

}
