package org.geekhub.student.web.controller;

import org.geekhub.student.dao.Student;
import org.geekhub.student.db.local.StudentsFromWeb;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.service.StudentRegistration;
import org.geekhub.student.service.StudentService;
import org.geekhub.student.service.StudentTimeFileManager;
import org.geekhub.student.service.abstractions.StudentManager;
import org.geekhub.student.util.logger.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/students")
public class StudentController {

    private static final Logger logger = new Logger(StudentController.class.getName());

    private final StudentTimeFileManager studentTimeFileManager;
    private final StudentRegistration studentRegistration;
    private final StudentService studentService;
    private final StudentsFromWeb studentsFromWeb;
    private final StudentManager studentManager;

    public StudentController(
            StudentTimeFileManager studentTimeFileManager,
            StudentRegistration studentRegistration,
            StudentService studentService,
            StudentsFromWeb studentsFromWeb,
            StudentManager studentManager
    ) {
        this.studentTimeFileManager = studentTimeFileManager;
        this.studentRegistration = studentRegistration;
        this.studentService = studentService;
        this.studentsFromWeb = studentsFromWeb;
        this.studentManager = studentManager;
    }

    @GetMapping
    public ResponseEntity<Map<GradeType, List<Student>>> main() {

        Map<GradeType, List<Student>> students =
                studentTimeFileManager.readFromTimeFile(Collections.emptyMap());

        if (students.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/analytics")
    public ResponseEntity<List<String>> getAnalytics(HttpServletRequest req) {
        return new ResponseEntity<>(studentService.getAnalytics(), HttpStatus.OK);
    }

    @PostMapping
    public void addStudent(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "type") String type,
            @RequestParam(name = "value") String value,
            @RequestParam(name = "date") String date,
            HttpServletResponse response
    ) {
        studentsFromWeb.addStudent(name, type, value, date);

        response.setStatus(HttpServletResponse.SC_ACCEPTED);
    }

    @PostMapping("/registration")
    public ResponseEntity<String> startStudentsRegistration(
            @RequestParam(name = "request") String request,
            @RequestParam(name = "totalStudentsCount") String totalStudentsCount
    ) {
        if (request.equals("manual")) {
            studentRegistration.startStudentRegistration(0);

            return new ResponseEntity<>(HttpStatus.CREATED);
        } else if (request.equals("auto")) {
            try {
                studentRegistration.startAutoStudentRegistration(Integer.parseInt(totalStudentsCount));

                return new ResponseEntity<>(HttpStatus.CREATED);
            } catch (NumberFormatException e) {
                logger.warning(new IllegalArgumentException("Attempting to send an invalid number of students!"));
            }
        }

        return new ResponseEntity<>("Attempting to send an invalid number of students!", HttpStatus.BAD_REQUEST);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping
    public void deleteStudents(HttpServletResponse response) {
        studentManager.deleteAllStudents();
        studentTimeFileManager.delete();

        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

}
