package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.service.abstractions.StudentOutput;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class StudentPrintStorage implements StudentOutput {

    private final StudentTimeFileManager studentTimeFileManager;

    public StudentPrintStorage(StudentTimeFileManager studentTimeFileManager) {
        this.studentTimeFileManager = studentTimeFileManager;
    }

    @Override
    public void printTableList(Map<GradeType, List<Student>> students) {
        studentTimeFileManager.writeToTimeFile(students);
    }

    @Override
    public void printTableList(List<Student> students) {

    }

}
