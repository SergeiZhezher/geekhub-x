package org.geekhub.student.web.controller.error;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorHandlerController implements ErrorController {

    @GetMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        int statusCode = 0;

        if (status != null) {
            statusCode = Integer.parseInt(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "/error-pages/error-404";
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return "/error-pages/error-403";
            }
        }
        model.addAttribute("errorMessage", statusCode);

        return "/error-pages/error";
    }

    @Override
    public String getErrorPath() {
        return "/error-pages/error-404";
    }
}
