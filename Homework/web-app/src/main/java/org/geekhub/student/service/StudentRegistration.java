package org.geekhub.student.service;

import org.geekhub.student.StudentsRegistry;
import org.geekhub.student.db.local.StudentsFromWeb;
import org.springframework.stereotype.Service;

@Service
public class StudentRegistration {

    private final StudentTimeFileManager studentTimeFileManager;
    private final StudentsRegistry studentsRegistry;
    private final StudentGenerator studentGenerator;
    private final StudentRecipient studentRecipient;
    private final StudentsFromWeb studentsFromWeb;
    private final StudentService studentService;

    public StudentRegistration(
            StudentTimeFileManager studentTimeFileManager,
            StudentsRegistry studentsRegistry,
            StudentGenerator studentGenerator,
            StudentRecipient studentRecipient,
            StudentsFromWeb studentsFromWeb,
            StudentService studentService
    ) {
        this.studentTimeFileManager = studentTimeFileManager;
        this.studentsRegistry = studentsRegistry;
        this.studentGenerator = studentGenerator;
        this.studentRecipient = studentRecipient;
        this.studentsFromWeb = studentsFromWeb;
        this.studentService = studentService;
    }

    public void startAutoStudentRegistration(int totalStudentsCount) {
        studentsRegistry.startStudentRegistration(totalStudentsCount, studentGenerator);
        studentService.updateAnalytics();
    }

    public void startStudentRegistration(int totalStudentsCount) {
        if (!studentsFromWeb.getStudentList(totalStudentsCount).isEmpty()) {

            studentTimeFileManager.writeToTimeFile(studentsFromWeb.getStudentList(totalStudentsCount));
            studentsFromWeb.deleteAll();

            studentsRegistry.startStudentRegistration(totalStudentsCount, studentRecipient);
            studentService.updateAnalytics();
        }
    }

}
