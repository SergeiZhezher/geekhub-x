package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.service.abstractions.StudentSource;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class StudentRecipient implements StudentSource {

    private final StudentTimeFileManager studentTimeFileManager;

    public StudentRecipient(StudentTimeFileManager studentTimeFileManager) {
        this.studentTimeFileManager = studentTimeFileManager;
    }

    @Override
    public List<Student> getStudentList(int totalStudentsCount) {
        return studentTimeFileManager.readFromTimeFile(Collections.emptyList());
    }

}
