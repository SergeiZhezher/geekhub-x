package org.geekhub.student.service;

import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.service.GradeFactoryInitialization;
import org.geekhub.student.service.analytics.StudentAnalytic;
import org.geekhub.student.util.cache.StudentAnalyticsCacheProvider;
import org.geekhub.student.util.exceptions.MultithreadingException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StudentService {

    private final ExecutorService executor = Executors.newFixedThreadPool(4);

    private final StudentAnalyticsCacheProvider studentAnalyticsCacheProvider;
    private final GradeFactoryInitialization gradeFactoryInitialization;
    private final StudentAnalytic studentAnalytic;

    public StudentService(
            StudentAnalyticsCacheProvider studentAnalyticsCacheProvider,
            GradeFactoryInitialization gradeFactoryInitialization,
            StudentAnalytic studentAnalytic
    ) {
        this.studentAnalyticsCacheProvider = studentAnalyticsCacheProvider;
        this.gradeFactoryInitialization = gradeFactoryInitialization;
        this.studentAnalytic = studentAnalytic;
    }

    public void updateAnalytics() {
        studentAnalyticsCacheProvider.setCachedAnalytics(calculateAnalytics());
    }

    public List<String> getAnalytics() {
        List<String> cachedAnalytics = studentAnalyticsCacheProvider.getCachedAnalytics();

        if (cachedAnalytics.isEmpty()) {
            studentAnalyticsCacheProvider.setCachedAnalytics(calculateAnalytics());
            cachedAnalytics = studentAnalyticsCacheProvider.getCachedAnalytics();

            return cachedAnalytics;
        }

        return cachedAnalytics;
    }

    private List<String> calculateAnalytics() {
        List<String> analytics = new ArrayList<>();

        try {
            for (GradeType value : GradeType.values()) {
                Future<String> averageScore = getAverageScore(value);
                Future<String> maxScore = getMaxScore(value);
                Future<String> minScore = getMinScore(value);
                Future<String> medianScore = getMedianScore(value);

                analytics.add(value.name() + " average score: " + averageScore.get());
                analytics.add(value.name() + " max score: " + maxScore.get());
                analytics.add(value.name() + " min score: " + minScore.get());
                analytics.add(value.name() + " median score: " + medianScore.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
            throw new MultithreadingException("Parallel get analytics gave an error: " + e.getMessage());
        }

        return analytics;
    }

    private Future<String> getAverageScore(GradeType value) {
        return executor.submit(() -> studentAnalytic.averageScore(value.name()).map(this::getValues).orElse("N/A"));
    }

    private Future<String> getMaxScore(GradeType value) {
        return executor.submit(() -> studentAnalytic.maxScore(value.name()).map(this::getValues).orElse("N/A"));
    }

    private Future<String> getMinScore(GradeType value) {
        return executor.submit(() -> studentAnalytic.minScore(value.name()).map(this::getValues).orElse("N/A"));
    }

    private Future<String> getMedianScore(GradeType value) {
        return executor.submit(() -> studentAnalytic.medianScore(value.name()).map(this::getValues).orElse("N/A"));
    }

    private String getValues(int value) {
        return Stream.of(GradeType.values())
                .map(gradeType -> gradeFactoryInitialization.createGrade(gradeType, value))
                .map(Grade::asPrintVersion)
                .collect(Collectors.joining(", ", "[", "]"));
    }

}
