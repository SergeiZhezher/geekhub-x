package org.geekhub.student.web.controller.error;

import org.geekhub.student.exception.UserAlreadyExistsException;
import org.geekhub.student.util.exceptions.MultithreadingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class ExceptionController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<String> entityNotFoundException(EntityNotFoundException exception) {
        return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = UserAlreadyExistsException.class)
    public ResponseEntity<String> userAlreadyExistsException(UserAlreadyExistsException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = MultithreadingException.class)
    public ResponseEntity<String> multithreading(MultithreadingException exception) {
       logger.error(exception.getMessage(), exception);

        return new ResponseEntity<>("Analytics error", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
