package org.geekhub.student.db.local;

import org.geekhub.student.dao.Student;
import org.geekhub.student.service.InputDataChecker;
import org.geekhub.student.service.abstractions.StudentSource;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
public class StudentsFromWeb implements StudentSource {

    private final List<Student> students = new ArrayList<>();

    private final InputDataChecker inputDataChecker;

    private StudentsFromWeb(InputDataChecker inputDataChecker) {
        this.inputDataChecker = inputDataChecker;
    }

    public List<Student> getStudentList(int totalStudentsCount) {
        return students;
    }

    public void addStudent(String studentName, String studentGradeType, String studentGradeValue, String studentDateTime) {

        boolean isValid = inputDataChecker
                .isValidInputData(studentName, studentGradeType, studentGradeValue, studentDateTime);

        if (isValid) {
            students.add(new Student(studentName,
                    inputDataChecker.getGrade(studentGradeType, inputDataChecker.getIntegerNumber(studentGradeValue)),
                    inputDataChecker.getValidDateTime(studentDateTime)
            ));
            return;
        }

        throw new ResponseStatusException(BAD_REQUEST);
    }

    public void deleteAll() {
        students.clear();
    }

}
