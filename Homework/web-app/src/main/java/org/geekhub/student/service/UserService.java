package org.geekhub.student.service;

import org.geekhub.student.dao.Role;
import org.geekhub.student.dao.User;
import org.geekhub.student.exception.UserAlreadyExistsException;
import org.geekhub.student.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<User> getUsers() {
        List<User> users = userRepository.findAll();

        if (users.isEmpty()) {
            throw new EntityNotFoundException();
        }

        return users;
    }

    public ResponseEntity<Void> addUser(String name, String password, String confirmedPassword) {
        if (!password.equals(confirmedPassword) || password.isBlank() || name.isBlank()) {
            return new ResponseEntity<>(BAD_REQUEST);
        }
        if (userRepository.existsByName(name)) {
            throw new UserAlreadyExistsException("Such user already exists!");
        }
        userRepository.save(new User(
                name,
                bCryptPasswordEncoder.encode(password),
                Collections.singleton(Role.USER),
                true
        ));

        return new ResponseEntity<>(CREATED);
    }

    public ResponseEntity<Void> deleteUser(User usr, User current) {
        if (usr.getId().equals(current.getId())) {
            return new ResponseEntity<>(FORBIDDEN);
        }
        userRepository.deleteById(usr.getId());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Void> updateRole(String role, User user, User current) {
        if (user.getId().equals(current.getId())) {
            return new ResponseEntity<>(FORBIDDEN);
        }
        user.getRoles().clear();
        user.getRoles().add(Role.valueOf(role));

        userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
