package org.geekhub.student.web.controller;

import org.geekhub.student.dao.Role;
import org.geekhub.student.dao.User;
import org.geekhub.student.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@PreAuthorize("hasAnyAuthority('ADMIN')")
public class UserManagerController {

    private final UserService userService;

    public UserManagerController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<Void> addUser(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "confirmedPassword") String confirmedPassword
    ) {
        return userService.addUser(name, password, confirmedPassword);
    }

    @PatchMapping("/users")
    public ResponseEntity<Void> updateRole(
            @AuthenticationPrincipal User current,
            @RequestParam(name = "id") User usr,
            @RequestParam(name = "role") String role
    ) {
        return Stream.of(Role.values()).map(Enum::name).collect(Collectors.toList()).contains(role) ?
                userService.updateRole(role, usr, current) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/users")
    public ResponseEntity<Void> deleteUser(
            @RequestParam(name = "id") User usr,
            @AuthenticationPrincipal User current
    ) {
        return userService.deleteUser(usr, current);
    }

}
