package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class StudentRecipientTest {

    private List<Student> studentGroup;

    private final StudentConsoleReader studentConsoleReader = new StudentConsoleReader() {
        @Override
        public String getValueFromConsole(String value, Scanner scanner) {
            if (value.toLowerCase().contains("name")) {
                return "userName";
            } else if (value.toLowerCase().contains("type")) {
                return GradeType.values()[new Random().nextInt(GradeType.values().length)].name();
            } else if (value.toLowerCase().contains("value")) {
                return "100";
            } else if (value.toLowerCase().contains("time")) {
                return "2077-01-01T10:09:09";
            }
            throw new IllegalArgumentException();
        }
    };

    @BeforeEach
    void setUp() {
        StudentRecipient studentRecipient = new StudentRecipient(studentConsoleReader);
        studentGroup = studentRecipient.getStudentList(2);
    }

    @Test
    void getStudentListLengthTest() {
        int sizeGroup = 2;
        assertEquals(sizeGroup, studentGroup.size());
    }

    @Test
    void getStudentListNameTest() {
        assertNotNull(studentGroup.get(0).getName());
    }

    @Test
    void getStudentListGradeTest() {
        assertNotNull(studentGroup.get(0).getGrade());
    }

    @Test
    void getStudentListGradeAsStringTest() {
        assertNotNull(studentGroup.get(0).getGradeAsString());
    }

    @Test
    void getStudentListGradeTimeTest() {
        assertNotNull(studentGroup.get(0).getGradeTime());
    }
}
