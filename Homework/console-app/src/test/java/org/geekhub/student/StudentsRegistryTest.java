package org.geekhub.student;

import org.geekhub.student.db.remote.h2.StudentH2Manager;
import org.geekhub.student.db.remote.postgres.StudentPostgresManager;
import org.geekhub.student.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.Mockito.*;

class StudentsRegistryTest {

    private final int totalStudentsCount = 1;

    @Mock
    private StudentRecipient studentRecipient;

    @Mock
    private StudentGenerator studentGenerator;

    @Mock
    private StudentGrouper studentGrouper;

    @Mock
    private StudentSorter studentSorter;

    @Mock
    private StudentPrinter studentPrinter;

    @Mock
    private StudentH2Manager studentH2Manager;

    private StudentsRegistry studentsRegistry;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        studentsRegistry = new StudentsRegistry(studentGrouper, studentSorter, studentH2Manager, studentPrinter);

        when(studentRecipient.getStudentList(totalStudentsCount)).thenReturn(Collections.emptyList());
        when(studentGenerator.getStudentList(totalStudentsCount)).thenReturn(Collections.emptyList());

        when(studentGrouper.getGroupedStudents(anyList())).thenReturn(Collections.emptyMap());
        when(studentSorter.getSortedListByGrade(anyMap())).thenReturn(Collections.emptyMap());
        when(studentSorter.getSortedListByName(anyList())).thenReturn(Collections.emptyList());

    }

    @Test
    void startStudentRegistrationTest() {
        studentsRegistry.startStudentRegistration(totalStudentsCount, studentRecipient);

        verify(studentRecipient, atLeastOnce()).getStudentList(totalStudentsCount);
        verify(studentGrouper, atLeastOnce()).getGroupedStudents(anyList());

        verify(studentSorter, atLeastOnce()).getSortedListByGrade(anyMap());
        verify(studentSorter, atLeastOnce()).getSortedListByName(anyList());

        verify(studentPrinter, atLeastOnce()).printTableList(anyList());
        verify(studentPrinter, atLeastOnce()).printTableList(anyMap());
    }

    @Test
    void startStudentAutoRegistration() {
        studentsRegistry.startStudentRegistration(totalStudentsCount, studentGenerator);

        verify(studentGenerator, atLeastOnce()).getStudentList(totalStudentsCount);
        verify(studentGrouper, atLeastOnce()).getGroupedStudents(anyList());

        verify(studentSorter, atLeastOnce()).getSortedListByGrade(anyMap());
        verify(studentSorter, atLeastOnce()).getSortedListByName(anyList());

        verify(studentPrinter, atLeastOnce()).printTableList(anyList());
        verify(studentPrinter, atLeastOnce()).printTableList(anyMap());
    }

}
