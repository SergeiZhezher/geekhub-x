package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.service.analytics.StudentAnalytic;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class StudentPrinterTest {

    private ByteArrayOutputStream consoleContent;
    private StudentPrinter studentPrinter;

    @Mock
    private Map<GradeType, List<Student>> students;

    @Mock
    private Student student;

    @Mock
    private StudentAnalytic studentAnalytic;

    @BeforeEach
    public void setUp() {
        consoleContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(consoleContent));

        studentPrinter = new StudentPrinter(studentAnalytic);

        MockitoAnnotations.openMocks(this);
    }

    @Test
    void printTableListByGradeTest() {
        studentPrinter.printTableList(students);
        assertNotNull(consoleContent);
    }

    @Test
    void printTableListByNameTest() {
        studentPrinter.printTableList(List.of(student, student));
        assertNotNull(consoleContent);
    }

    @AfterEach
    void finishUp() {
        System.setOut(System.out);
    }
}



