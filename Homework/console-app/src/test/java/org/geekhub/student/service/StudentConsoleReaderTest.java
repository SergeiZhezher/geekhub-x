package org.geekhub.student.service;

import org.geekhub.student.util.exceptions.ParameterNotSpecifiedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StudentConsoleReaderTest {

    private StudentConsoleReader consoleReader;

    @BeforeEach
    private void setUp() {
        consoleReader = new StudentConsoleReader();
    }

    @Test
    void getValueFromConsoleTest() {
        String str = "test";
        assertEquals(str, consoleReader.getValueFromConsole(str, new Scanner(str)));
    }

    @Test()
    void failGetValueFromConsoleTest() {
        assertThrows(ParameterNotSpecifiedException.class, () -> consoleReader.getValueFromConsole("test", new Scanner(" ")));
    }

}