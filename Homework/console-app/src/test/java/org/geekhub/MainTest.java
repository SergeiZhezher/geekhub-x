package org.geekhub;

import org.geekhub.student.util.exceptions.InvalidStartupArgumentException;
import org.geekhub.student.util.modes.ProgramMode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MainTest {

    @Test
    void wrong_number_of_arguments_passed_test() {
        assertThrows(InvalidStartupArgumentException.class, () -> Main.main(new String[1]));
        assertThrows(InvalidStartupArgumentException.class, () -> Main.main(new String[3]));
    }

    @Test
    void startup_argument_not_an_integer_number_test() {
        String[] args = {"Not integer", ProgramMode.MANUAL.name()};
        assertThrows(InvalidStartupArgumentException.class, () -> Main.main(args));
    }

    @Test
    void non_existent_mod_specified_test() {
        String[] args = {"10", ProgramMode.NONE.name()};
        assertThrows(InvalidStartupArgumentException.class, () -> Main.main(args));
    }
}
