package org.geekhub;

import org.geekhub.student.StudentsRegistry;
import org.geekhub.student.service.StudentGenerator;
import org.geekhub.student.service.StudentRecipient;
import org.geekhub.student.util.exceptions.InvalidStartupArgumentException;
import org.geekhub.student.util.modes.ProgramMode;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Main {

    private static StudentsRegistry studentsRegistry;
    private static StudentRecipient studentRecipient;
    private static StudentGenerator studentGenerator;

    public Main(
            StudentsRegistry studentsRegistry,
            StudentRecipient studentRecipient,
            StudentGenerator studentGenerator

    ) {
        Main.studentsRegistry = studentsRegistry;
        Main.studentRecipient = studentRecipient;
        Main.studentGenerator = studentGenerator;
    }

    public static void main(String[] args) {
        int totalStudentsCount = getStudentsCountArgument(args);
        String inputMode = getStudentInputMode(args);

        new SpringApplicationBuilder(Main.class)
                .web(WebApplicationType.NONE)
                .run(args);

        startMainApp(totalStudentsCount, inputMode);
    }

    private static void startMainApp(int totalStudentsCount, String inputMode) {
        if (inputMode.equals(ProgramMode.MANUAL.name())) {
            studentsRegistry.startStudentRegistration(totalStudentsCount, studentRecipient);
        }

        if (inputMode.equals(ProgramMode.RANDOM.name())) {
            studentsRegistry.startStudentRegistration(totalStudentsCount, studentGenerator);
        }
    }

    private static String getStudentInputMode(String[] args) {
        if (!args[1].equals(ProgramMode.MANUAL.name()) && !args[1].equals(ProgramMode.RANDOM.name())) {
            throw new InvalidStartupArgumentException("Non-existent mod specified");
        }
        return args[1];
    }

    private static int getStudentsCountArgument(String[] args) {
        if (args.length == 3) {
            throw new InvalidStartupArgumentException("The database must be specified in the property");
        }
        if (args.length != 2) {
            throw new InvalidStartupArgumentException("Wrong number of arguments passed");
        }
        String count = args[0];

        try {
            return Integer.parseInt(count);
        } catch (NumberFormatException e) {
            throw new InvalidStartupArgumentException("Startup argument not an integer number: " + count);
        }
    }

}
