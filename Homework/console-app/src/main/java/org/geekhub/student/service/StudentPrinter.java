package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.grades.GradeType;
import org.geekhub.student.grades.service.Grade;
import org.geekhub.student.grades.service.GradeFactoryInitialization;
import org.geekhub.student.service.abstractions.StudentOutput;
import org.geekhub.student.service.analytics.StudentAnalytic;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class StudentPrinter implements StudentOutput {

    private final StudentAnalytic studentAnalytic;

    public StudentPrinter(StudentAnalytic studentAnalytic) {
        this.studentAnalytic = studentAnalytic;
    }

    @Override
    public void printTableList(Map<GradeType, List<Student>> students) {
        students.forEach((gradeType, list) -> {
            if (!list.isEmpty()) {
                System.out.println("\nStudents with " + gradeType + " Grade");
                printTableHeader();

                for (Student s : list) {
                    System.out.println(getFormattedString(s.getName(), s.getGradeAsString(), s.getGradeTime()));
                }

                printBottomTable();
                printAnalytics(list, gradeType);
            }
        });
    }

    @Override
    public void printTableList(List<Student> students) {
        System.out.println("\nStudents sorted by Name");
        printTableHeader();

        for (Student s : students) {
            System.out.println(getFormattedString(s.getName(), s.getGradeAsString(), s.getGradeTime()));
        }

        printBottomTable();
    }

    private void printTableHeader() {
        System.out.println("-".repeat(77));
        /** This regex just make spaces between words and add delimiters - '|' */
        System.out.format("| %-30s| %-20s| %-20s|\n", "Name", "Grade", "date-time");
        System.out.println("-".repeat(77));
    }

    private String getFormattedString(String name, String grade, LocalDateTime gradeTime) {
        /** This regex just make spaces between words and add delimiters - '|' */
        return String.format("| %-30s| %-20s| %-20s|", name, grade, gradeTime);
    }

    private void printBottomTable() {
        System.out.println("-".repeat(77));
    }

    private void printAnalytics(List<Student> students, GradeType gradeType) {
        if (!students.isEmpty()) {
            String type = students.get(0).getGrade().getClass().getSimpleName();

            System.out.println("Average score in '" + gradeType + "' is "
                    + studentAnalytic.averageScore(type).map(this::getValues).orElse("N/A"));
            System.out.println("Max score in '" + gradeType + "' is "
                    + studentAnalytic.maxScore(type).map(this::getValues).orElse("N/A"));
            System.out.println("Min score in '" + gradeType + "' is "
                    + studentAnalytic.minScore(type).map(this::getValues).orElse("N/A"));
            System.out.println("Median score in '" + gradeType + "' is "
                    + studentAnalytic.medianScore(type).map(this::getValues).orElse("N/A"));
        }
    }

    private String getValues(int value) {
        GradeFactoryInitialization gradeFactoryInitialization = new GradeFactoryInitialization();
        return Stream.of(GradeType.values())
                .map(gradeType -> gradeFactoryInitialization.createGrade(gradeType, value))
                .map(Grade::asPrintVersion)
                .collect(Collectors.joining(", ", "[", "]"));
    }
}
