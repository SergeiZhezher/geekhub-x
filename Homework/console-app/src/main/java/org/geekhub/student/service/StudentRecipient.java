package org.geekhub.student.service;

import org.geekhub.student.dao.Student;
import org.geekhub.student.service.abstractions.StudentSource;
import org.geekhub.student.util.exceptions.GradeNotSupportedException;
import org.geekhub.student.util.exceptions.InvalidInputException;
import org.geekhub.student.util.exceptions.InvalidScoreException;
import org.geekhub.student.util.exceptions.ParameterNotSpecifiedException;
import org.geekhub.student.util.logger.Logger;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class StudentRecipient extends InputDataChecker implements StudentSource {

    private final StudentConsoleReader consoleReader;
    private final Logger logger = new Logger(StudentRecipient.class.getName());

    public StudentRecipient(StudentConsoleReader studentConsoleReader) {
        this.consoleReader = studentConsoleReader;
    }

    @Override
    public List<Student> getStudentList(int totalStudentsCount) {
        List<Student> studentList = new ArrayList<>();

        try (Scanner scanner = new Scanner(System.in)) {
            for (int i = 0; i < totalStudentsCount; i++) {
                studentList.add(getNewStudent(scanner));
            }
        }

        return studentList;
    }

    private Student getNewStudent(Scanner scanner) {
        String name = "";
        String gradeType = "";
        int gradeValue = -1;

        while (true) {
            try {
                if (name.isBlank()) {
                    name = getStudentName(scanner);
                }
                if (gradeType.isBlank()) {
                    gradeType = getGradeType(scanner);
                }
                if (gradeValue == -1) {
                    gradeValue = getGradeValue(scanner);
                }

                return new Student(name, getGrade(gradeType, gradeValue), getValidDateTime(scanner));

            } catch (ParameterNotSpecifiedException e) {
                logger.warning(e);
            }
        }
    }

    private String getStudentName(Scanner scanner) {
        return consoleReader.getValueFromConsole("Name", scanner);
    }

    private String getGradeType(Scanner scanner) {
        String gradeType = consoleReader.getValueFromConsole("Grade type - only (GPA, LETTER, PERCENTAGE, UKRAINIAN) ", scanner);

        while (true) {
            try {
                gradeType = getExistType(gradeType);
                break;
            } catch (GradeNotSupportedException e) {
                logger.warning(e);
                gradeType = consoleReader.getValueFromConsole("Grade type - only (GPA, LETTER, PERCENTAGE, UKRAINIAN)", scanner);
            }
        }
        return gradeType;
    }

    private int getGradeValue(Scanner scanner) {
        String gradeValue = consoleReader.getValueFromConsole("Grade value - only (ranging from 0 to 100) ", scanner);
        int value;
        while (true) {
            try {
                value = getIntegerNumber(gradeValue);
                value = getExistGrade(value);
                break;
            } catch (InvalidScoreException e) {
                logger.warning(e);
                gradeValue = consoleReader.getValueFromConsole("Grade value - only (ranging from 0 to 100)", scanner);
            } catch (InvalidInputException e) {
                logger.warning(e);
                gradeValue = consoleReader.getValueFromConsole("Grade value - only ( ranging from 0 to 100)", scanner);
            }
        }
        return value;
    }

    private LocalDateTime getValidDateTime(Scanner scanner) {
        LocalDateTime localDateTime;

        while (true) {
            try {
                localDateTime = getStudentDateTime(scanner);
                break;
            } catch (DateTimeParseException e) {
                logger.warning(new InvalidInputException("LocalDateTime entered incorrectly"));
            }
        }

        return localDateTime;
    }

    private LocalDateTime getStudentDateTime(Scanner scanner) {
        String dateTime = consoleReader.getValueFromConsole("Date-time - (2077-01-01T10:09:09)", scanner);
        return LocalDateTime.parse(dateTime);
    }

}
