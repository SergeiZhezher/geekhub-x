package org.geekhub.student.util.exceptions;

public class ParameterNotSpecifiedException extends IllegalArgumentException {
    public ParameterNotSpecifiedException(String message) {
        super(message);
    }
}
