package org.geekhub.student.service;

import org.geekhub.student.util.exceptions.ParameterNotSpecifiedException;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class StudentConsoleReader {

    public String getValueFromConsole(String value, Scanner scanner) {

        System.out.println(value + ": ");
        value = scanner.nextLine();

        if (value.isBlank()) {
            System.out.println("Field should be non-empty, please enter again");
            throw new ParameterNotSpecifiedException("Data was not entered");
        }

        return value;
    }

}
