package org.geekhub.student.util.exceptions;

public class InvalidStartupArgumentException extends IllegalArgumentException {
    public InvalidStartupArgumentException(String message) {
        super(message);
    }
}
